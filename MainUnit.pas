unit MainUnit;

// 2016.11.19 Delphi XE8 ������
// 2013.12.24 (��� ������� �� dpr � ��������� unit, ����� �������� ������ � Ctrl+Click � Delphi 6)
// 2010.02.24 h (��������� ��������� Extended �������� ����� Asphyre �������������).
// �������� ������ "�������� �� ������������"
// �����: ������� "Corris" �������
// email: corriscant@mail.ru
// ��������: ������ ������ 

// ������: �������� ���������-����, � ������� ����� ������� �������� ������������
// (����� ��� � ������ ���� paratrooper), ��������� � �������� �������� ��� ���������� ���.
// ����� ���������� ������� ������!

// ���� ���� - �� ��������� ������� �� ����� �� ������ ����������� �� ������ �� ���������.
// ���� ��� ���������� ������� ������ ���� ����������� ������ ������ �� ����� ������ ������� ��� �� ���������.
// ������ ��������� ������������� ������ ��������� �����, �������� ������� ������� � �������� � 100 ���.

// � ����������� ���� ����������� ������������� ������, � ������� ����������� ����� ������� �� �����,
// � ������� ����������� ���������� Asphyre.
// ������ ����, ��� � ��� ������ ���������� �������� � ����� Parashut_Solution.exe

// �������������� ���������� � ���������:

// 1. � ������� ������ ������ ������ 600�� (�������� ����������), � ��������� ������� � �� ��������� ��������� (15-40 �������� � �������)
// ������ ������ �����������, �������������� ������������ ParatrooperImage.
// 2. ����� �������� �������������, � �������� ���� ������� � 600 �� (�������� ������ ���������� ��� ������� ������� ������������).
// 3. ����� �� ���������������� �� ������� � �������� ������������, �� ���� ��������� ������ �������� � ��� �������,
// ��� ����� �������� ���� ��� ���� ��� ������ �����������.
// 4. ����� �������� �������� ������ ����� �� ������ ��� ��������� ���� ������������.

// 5. ������ ������������ ������ ������ ���� �������� ������ ������ (�������� ����). ������� �� �����������, ����� �� ������.
// ������ - �������� ������, �� ���������� ��������, �� ������ �������������� ������������ BulletImage.
// 6. �������� ������ ������� ������� �������� (30-100 �������� � �������),
// � ������ �������������� ��������� ������� ����� ���������.
// 7. �� ������� ����������� ������ ������������� ������ ���� ������.

// ����� ������� �� ������ ������ ������ - ��������� ������ ������� � ������. ����� �� �����
// ������������� �������� �������� ����������� ��������, ���������� ������ ������ ����������� �������� ����� ������� � �����������.

// 8. ��� ��������� ������� � �����������-���� ��� ������� ������ ������������,
// ���� ��������� �� ��������� - �� ������� ����������� �� ����� �����������.
// 9. ������ ������ ���������� ������ ���� ����, �� ���� �� �������� ��������� ����� ������ ������������ �� ���� � ������.
// 10. ��� ������� �� ������ "�����" � "������" ����� ������ ������������ �� �����������, �� � �������� ������.
// 11. ��� ������ ������ ������� �� ������� ������ �� ������ ������������.
// 12. ������ ���� ������������� ����������� ���� �������� ��� ������ �� ���������, ��� ����� ������ ������� ������ ������.

// ��������� ������� ����������� �������� � �� ��������������.
// ��������� ������ ���� �������� � ����������� ����������� ����������� ���.
// ��� �������, �������������� �� ������ ������ ����������� ������� ����� TBaseObject.
// ������ ������ ������ ����������� � ������������������� � ��������������� ��� ������������, � ������������ ����� ����������.
// ����� �������� ����������� ��������� ������ ����������� ����� �� ������, ��������, �������� ����� ����� Move,
// � ���������� �� ������ ����� ����� Draw.
// ����� ������� - ������� ��� ���� ����� ���������� � �����������.
// ��������� ������ ���� �������� ��� ������ ������ (�� ��������, ����������, ������ � Asphyre ����������, ������� �� �������������� �� ������).
// ��� �� ������ ��������� ����� ������ � Warning'��, ������� ����� �������� � ������� ��� ���������� ���������� ���������.
// ������ ����������� �������� � ��������������� ����.
// ����������� ��������� ������������� ������������, �� ���� �������, ��������, � ����� �������� ���������� � ����������.

// ����������:
// �����
// procedure TAsphyreMainForm.AsphyreDeviceRender(Sender: TObject);
//   -> �������� �� ����������� ����� ����������� �� �����
// procedure TAsphyreMainForm.PhysicsTimerOnTime;
//   -> �������� �� ����������� �������� ������

interface
 uses
  Forms,
  Classes,
  Math,
  Windows,
  AsphyreCanvas,
  AsphyreTimers,
  AsphyreImages,
  AsphyreDb,
  AsphyreDef,
  AsphyreDevices,
  GameUtils,
  DispatcherUnit;

type
  // Asphyre Direct3D ����.
  TAsphyreMainForm = class(TForm)
    published
      constructor Create(AOwner : TComponent); override;
      procedure FormCreate(Sender: TObject);
      procedure FormDestroy(Sender: TObject);
      procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    public
      // Asphyre ����������
      AsphyreDevice: TAsphyreDevice;
      AsphyreCanvas: TAsphyreCanvas;
      AsphyreTimer: TAsphyreTimer;
      // ����� �������� �� ����������� ����� ����������� �� �����
      procedure AsphyreDeviceRender(Sender: TObject);
      procedure AsphyreTimerOnTimer(Sender: TObject);
      // ����� �������� �� ����������� �������� ������
      procedure PhysicsTimerOnTime;
    private
      function GunPosChecker(pos: RPoint): boolean;
      function ParatrooperOnAir(pos: RPoint): boolean;
      function InitialFallCoordGenerator(): RPoint;
      function InitialAmmoPos(): RPoint;
      function BulletOnScene(pos: RPoint): boolean;
      procedure ShotHit(Sender: TBaseObject; SenderTarget: TBaseObject);
      procedure ShotMiss(Sender: TBaseObject; SenderTarget: TBaseObject);
      function StartShootingCondition(): boolean;
  end;

CONST
  GUN_SPEED = 2; // �������� �������� �����
  GLOBAL_SPEED_NORM = 1000;
  PARATROOPER_SPAWN_INTERVAL = 600;  // ������� ��������� ������������
  BULLET_SPAWN_INTERVAL = 600;   // ������� ��������� ���������

var
  FormMain: TAsphyreMainForm;
  // �����������
  GunImage : TAsphyreImage; //< �����
  ParatrooperImage : TAsphyreImage; //< ����������
  BulletImage : TAsphyreImage; //< ����
  // �������
  Gun : TGun;
  FallingGenerator: TParatrooperGenerator; // ��������� ������������
  AmmoGenerator: TBulletGenerator; // ��������� ����
  GameDispatcher: TGameDispatcher; // ������, ������������ ���������� ������
  ShootingStarted: boolean;  // ����, ��� ���� �������� ��������
  
implementation

// ������ FreeAndNil, ���������� ������ � �������� ������
procedure Destnil(var AObject);
begin
  if Pointer(AObject) = nil then
    Exit;
  TObject(AObject).Destroy;
  pointer(AObject) := nil;
end;

// ������� ����������� (���������� ���������� � ������������ ��������� ���������)
procedure TAsphyreMainForm.AsphyreDeviceRender(Sender: TObject);
begin
  Gun.Draw(AsphyreCanvas);   // ������ �����
  GameDispatcher.DrawObjects(AsphyreCanvas);   // ������ ��� ���������� �������
end;

// ������� ������ (���������� ���������� � ������������ ��������� ���������)
procedure TAsphyreMainForm.PhysicsTimerOnTime;
begin
  GameDispatcher.IncrementMovements;
  if not ShootingStarted then
    begin
      ShootingStarted := StartShootingCondition;
      if ShootingStarted then
        AmmoGenerator.EnableSpawn(True);
    end;
end;

////////////////////////

// ������ ������, ����������� � ����������� ��������� ��������� ����������� ���������� Asphyre
// �� ������� ����������
procedure TAsphyreMainForm.AsphyreTimerOnTimer(Sender: TObject);
begin
  AsphyreDevice.Render(0, True); //< ��������� ������ �� ���� clNavy
  PhysicsTimerOnTime; //< �������� ������ ������ (������ ������)
  AsphyreDevice.Flip(); //< ������ ����� � �������
end;

constructor TAsphyreMainForm.Create(AOwner: TComponent);
begin
  // ������� ���������� ��������� ����������� CreateNew,
  CreateNew(AOwner);
  // ������ ��������� ����
  Width := 640;
  Height := 480;
  // ����������� ��������������� �������, ��� ��� ������� � dfm �����
  OnCreate := FormCreate;
  OnDestroy := FormDestroy;
end;

// �������������� AsphyreDevice, ��������� ��� ����������� ��������� � �������
procedure InitAsphyreDevice;
begin
  FormMain.AsphyreDevice := TAsphyreDevice.Create(FormMain);
  with FormMain.AsphyreDevice do
    begin
      // ���������
      BitDepth := bdHigh;
      DepthBuffer := false;
      HardwareTL := false;
      Height := 768;
      Name := 'AsphyreDevice';
      Refresh := 0;
      Tag := 0;
      VSync := false;
      Width := 1024;
      Windowed := true;
      WindowHandle := 0;
      // �������
      OnRender := FormMain.AsphyreDeviceRender; //< ��� ��������� ��� ������� ������
    end;
end;

// �������������� AsphyreCanvas, ��������� ��� ����������� ���������
procedure InitAsphyreCanvas;
begin
  FormMain.AsphyreCanvas := TAsphyreCanvas.Create(FormMain);
  with FormMain.AsphyreCanvas do
    begin
      // ���������
      // # �������� # - ����� 
      Publisher := FormMain.AsphyreDevice;
      AlphaTesting := true;
      Antialias := true;
      Dithering := true;
      Name := 'AsphyreCanvas';
      Tag := 0;
      VertexCache := 4096;
    end;
end;

// �������������� AsphyreTimer, ��������� ��� ����������� ��������� � �������
procedure InitAsphyreTimer;
begin
  FormMain.AsphyreTimer := TAsphyreTimer.Create(FormMain);
  with FormMain.AsphyreTimer do
    begin
      // ���������
      Enabled := true;
      MaxFPS := 1000;
      Name := 'AsphyreTimer';
      Speed := GLOBAL_SPEED_NORM;
      Tag := 0;
      // �������
      OnTimer := FormMain.AsphyreTimerOnTimer;
    end;
end;

// �������, ������������� ��� ������� �� ������
procedure TAsphyreMainForm.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  delta: RPoint;
begin
  case Key of
    VK_Left:
      begin
        delta.x := -GUN_SPEED;
        delta.y := 0;
        Gun.Move(delta);
      end;
    VK_Right:
      begin
        delta.x := GUN_SPEED ;
        delta.y := 0;
        Gun.Move(delta);
      end;
    VK_ESCAPE:
      Application.Terminate;
  end;
end;

// ������� �� OnCreate �����
procedure TAsphyreMainForm.FormCreate(Sender: TObject);
var
  xFile : TASDb; //< ���� �� ���������
  x : real;
  y : real;
begin
  // ����������� ������� �� ������������ ������� ������
  OnKeyDown := FormKeyDown;

  // �������������� ����������� �����
  InitAsphyreDevice;
  InitAsphyreCanvas;
  InitAsphyreTimer;

  // ���������� ����
  x := (Screen.Width div 2) - (Width div 2);
  y := (Screen.Height div 2) - (Height div 2);
  if AsphyreDevice.Windowed then // ���� ������� ����� - ����������
    begin
      Left := Round(x);
      Top := Round(y);
    end;

  // ��������� Asphyre, ����� ��������� ���� ����������
  AsphyreDevice.Initialize;

  // ��������� ���� �� ���������
  xFile := TASDb.Create(nil);
  xFile.FileName := 'images.asdb';
  // ������� ������ - ����������� ����� � ��������� �������� �� �����
  GunImage := TAsphyreImage.Create;
  GunImage.LoadFromASDb('gun.image', xFile);
  // ������� ������ - ����������� ����������� � ��������� �������� �� �����
  ParatrooperImage := TAsphyreImage.Create;
  ParatrooperImage.LoadFromASDb('paratrooper.image', xFile);
  // ������� ������ - ����������� ���� � ��������� �������� �� �����
  BulletImage := TAsphyreImage.Create;
  BulletImage.LoadFromASDb('bullet.image', xFile);
  xFile.Free;

  // ������� ������ - �����
  Gun := TGun.Create('Gun 01', GunImage);
  Gun.Coord.x :=  AsphyreDevice.Width/2;
  Gun.Coord.y :=  AsphyreDevice.Height - GunImage.VisibleSize.Y/2;
  Gun.SetPosConstraint(FormMain.GunPosChecker);

  FallingGenerator := TParatrooperGenerator.Create(ParatrooperImage);
  FallingGenerator.SetSpawnInterval(PARATROOPER_SPAWN_INTERVAL);
  FallingGenerator.EnableSpawn(True);

  AmmoGenerator := TBulletGenerator.Create(BulletImage);
  AmmoGenerator.SetFeeder(FallingGenerator.NextAim);
  AmmoGenerator.SetHitCallback(FormMain.ShotHit);
  //AmmoGenerator.SetMissCallback(FormMain.ShotMiss);
  AmmoGenerator.EnableSpawn(False);
  AmmoGenerator.SetSpawnInterval(BULLET_SPAWN_INTERVAL);

  GameDispatcher := TGameDispatcher.Create;
  GameDispatcher.AddGenerator(FallingGenerator,
      FormMain.InitialFallCoordGenerator, FormMain.ParatrooperOnAir);
  GameDispatcher.AddGenerator(AmmoGenerator,
      FormMain.InitialAmmoPos,FormMain.BulletOnScene);
  GameDispatcher.SetSpeedNorm(GLOBAL_SPEED_NORM);
  
  ShootingStarted := False;

  {$WARNINGS OFF}
  SetPrecisionMode(pmExtended);  //< ���������� ���������� �������� ��������
  {$WARNINGS ON}
end;

// ������� �� OnDestroy �����
procedure TAsphyreMainForm.FormDestroy(Sender: TObject);
begin
  // ���������� ������� �������
  Destnil(Gun);

  // ������� ������� �����������
  Destnil(GunImage);
  Destnil(ParatrooperImage);
  Destnil(BulletImage);

  // ����������� ��������������� ������� Asphyre ����������
  Destnil(AsphyreTimer);
  Destnil(AsphyreCanvas);
  Destnil(AsphyreDevice);

  Destnil(GameDispatcher);
end;

function TAsphyreMainForm.GunPosChecker(pos: RPoint): boolean;
begin
 Result := (pos.x >= 0) and (pos.x <= AsphyreDevice.Width);
end;

function TAsphyreMainForm.InitialFallCoordGenerator: RPoint;
begin
  Result.y := -2.5;
  Result.x := Random * AsphyreDevice.Width;
end;

function TAsphyreMainForm.ParatrooperOnAir(pos: RPoint): boolean;
begin
  Result := pos.y <= AsphyreDevice.Height;
end;

function TAsphyreMainForm.InitialAmmoPos: RPoint;
begin
  Result := Gun.Coord;
end;

function TAsphyreMainForm.BulletOnScene(pos: RPoint): boolean;
begin
  Result := (pos.x > 0) and (pos.x < AsphyreDevice.Width) and (pos.y > 0);
end;

procedure TAsphyreMainForm.ShotHit(Sender, SenderTarget: TBaseObject);
begin
  AmmoGenerator.RemoveObject(Sender);
  FallingGenerator.RemoveObject(SenderTarget);
end;

function TAsphyreMainForm.StartShootingCondition: boolean;
begin
  Result := FallingGenerator.Count > 5;
end;

procedure TAsphyreMainForm.ShotMiss(Sender, SenderTarget: TBaseObject);
begin
  FallingGenerator.RenewAsTarget(SenderTarget);
end;

end.
