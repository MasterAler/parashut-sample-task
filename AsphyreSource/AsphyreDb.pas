unit AsphyreDb;
//---------------------------------------------------------------------------
// AsphyreDb.pas                                        Modified: 16-Oct-2005
// Asphyre Secure Database (ASDb) implementation                  Version 1.0
//---------------------------------------------------------------------------
// The contents of this file are subject to the Mozilla Public License
// Version 1.1 (the "License"); you may not use this file except in
// compliance with the License. You may obtain a copy of the License at
// http://www.mozilla.org/MPL/
//
// Software distributed under the License is distributed on an "AS IS"
// basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
// License for the specific language governing rights and limitations
// under the License.
//---------------------------------------------------------------------------
interface

//---------------------------------------------------------------------------
uses
 Windows, Classes, Math, SysUtils, StreamEx, AsphyreData, Blowfish, AsphyreMD5, AsphyreDef, Graphics, SyncObjs, Types;

//---------------------------------------------------------------------------
const
 ASDbSignature = $62445341; // 'ASDb'

 // TRecordInfo.CompressionType values
 rctOldNoSecure = 0; //< ������ ������� asdb-���� ������ � ����������� secure
 rctOldSecure = 1; //< ������ ������� asdb-���� ������ � ���������� secure
 rctNoCompression = 2; //< ��� ���������� (����� � ������� �������� �������� ��� ������, ��� ������� ������������� �������� ���������)
 rctFastest = 3; //< �������� ������� �� ������� ���������
 rctMax = 4; //< ������ ������� � �������� ���������

 // record type enumerations
 recUnknown    = 0;
 recGraphics   = 1;
 recFile       = 2;
 recFont       = 3;
 recCustomData = 4;

 // ������� � ���� color format � ������ ���� ������ ���� recCustomData
 CustomDataFormat_AlphaForAimEncrypted = 23383;

//---------------------------------------------------------------------------
type
 PASDbHeader = ^TASDbHeader;
 TASDbHeader = packed record
  Signature  : Longword; // signature ('ASDb' = 62445341h)
  RecordCount: Longword; // number of records in the archive
  TableOffset: Longword; // table offset
 end;

//---------------------------------------------------------------------------
{
 ASDb Table structure:
  Key Name      -  4+ bytes (dword: length; [length-bytes]: string chars)
  Offset        -  unsigned dword

 ASDb Record structure:
  RecordType    -  word

  OrigSize      -  unsigned dword
  PhysSize      -  unsigned dword
  DateTime      -  double (unsigned qword)

  Checksum      -  16 bytes (MD5 message-digest)

  Encoding      -  word
  IV            -  unsigned qword (8 bytes) as IV

  DataBlock     -  DataSize bytes
}

//---------------------------------------------------------------------------
 TRecordInfo = record
  Key       : string;    // record unique identifier
  Offset    : Longword;  // record offset in archive
  RecordType: Cardinal;  // type of the record (generic, file, graphics, etc)
  OrigSize  : Cardinal;  // original data size
  PhysSize  : Cardinal;  // physical data size
  DateTime  : TDateTime; // record date & time

  Checksum  : array[0..3] of Longword; // MD5 message-digest

  // ��� ����������. ���� ��� ������ ���� ���� �� ��� ����� ���� �� old ��������
  // � ����� ���������� ������ ������� Secure
  CompressionType : Word;

  // ���� ������������, ����� �� ������������������(�� ���� ���, ��� ������� ���) � ����������� .asdb ������������ ������ ����������. ���� ����� true, ������ ������
  ForceCompressionType : boolean;

  // �� ��� ���� ����� ���������� ������ � ������ ���� compression type �� ����� � old ��������
  // � ����� ��� ����� ���������� ������ ������� InitBlock

  ColorFormat : Word;
  PatternCount : Word;
  PatternWidth : Word;
  PatternHeight : Word;
 end;

//---------------------------------------------------------------------------
 TOpenModes = (opUpdate, opOverwrite, opReadOnly);

  TAsphyreImageHeader = packed record
    InFormat : TColorFormat;
    PatternSize : TPoint;
    VisibleSize : TPoint;
    PatternCount : Integer;
    TexSize : TPoint;
    TexCount : Integer;
  end;

  TRecordInfoArray = array of TRecordInfo;

  TASDbCachedInfo_Item = class
    ASDbPath : string;
    RecordsInfo : TRecordInfoArray;
    constructor Create;
  end;

  {$IFDEF UseASDBCachedInfo}
  TASDbCachedInfo = class
    constructor Create;
    destructor Destroy; override;
  private
    Items : TList; // List<TASDbCachedInfo_Item>. ��� �� ���� Dictionary<string, TRectordInfoArray> � ������ - ���� � ����, ��������� - ������� ������ (= ���� ��� ������ ���������)
    CriticalSection : TCriticalSection;
  public
    // ������� ����� ������ � �������� ����� � ���������, ��� ���� � ��� ������ ������� ������ �� ��������� �������
    procedure AddRecordsInfo(ASDbPath : string; var AInfoArrayToGetFrom : TRecordInfoArray);
    // ������ ����� �� ��������� ���� � ���������, � ������ ������ (Result = true) � �������� ������ ������� ��������� �������
    function GetRecordsInfo(ASDbPath : string; var AInfoArrayToSetTo : TRecordInfoArray) : boolean;
    // ������ ��� ��������� � ASDbPath. ��� ����� ���� �� ��������� ���� �� ������, ���� ����� � ����� ��������� �� ������ (����� �� ������ ������ ���������� ��� � �� ������ ������ ��� �� ��������, �.�. ���� ������)
    procedure ClearRecordsInfo(ASDbPath : string);
  end;
  {$ENDIF}

//---------------------------------------------------------------------------
 PAsdb = ^TAsdb;
 TASDb = class(TComponent)
 private
  FDoCompareCheckSumm : boolean;

  {$IFDEF UseASDBCachedInfo}
  FRecordsReadOnce : boolean;
  {$ENDIF}

  FUpdatedOnce: Boolean;
  FFileSize : Cardinal;
  FFileName : string;
  FOpenMode : TOpenModes;
  FRecords  : TRecordInfoArray;
  ASDbHeader: TASDbHeader;

  PassBlock : array[0..56] of Byte;
  FPassSize : Integer;

  function GetRecordDate(Num: Integer): TDateTime;
  function GetRecordCount(): Integer;
  function GetRecordPhysSize(Num: Integer): Integer;
  function GetRecordKey(Num: Integer): string;
  function GetRecordNum(Key: string): Integer;
  function GetRecordOrigSize(Num: Integer): Integer;
  procedure SetFileName(const Value: string);
  function CreateEmtpyFile(): Boolean;
  function GetRecordType(Num: Integer): Integer;

  function GetRecordCompressionType(Num: Integer): Word;
  function GetRecordForceCompressionType(Num: Integer): Boolean;
  function GetRecordColorFormat(Num: Integer): Word;
  function GetRecordPatternCount(Num: Integer): Word;
  function GetRecordPatternWidth(Num: Integer): Word;
  function GetRecordPatternHeight(Num: Integer): Word;

  function GetPassword(): Pointer;

  function ReadASDbHeader(Stream: TStream; ASDbHeader: PASDbHeader): Boolean;
  function ReadASDbInfo(Stream: TStream): Boolean;
  function WriteRecordTable(): Boolean;

  class function CompressData(Source: Pointer; SourceSize: Cardinal;
   out Data: Pointer; out DataSize: Cardinal; ACompression : TCompressionLevel = clHighest): Boolean;
  class function DecompressData(Source: Pointer; SourceSize: Cardinal;
   out Data: Pointer; DataSize: Cardinal): Boolean;
    function GetRecordChecksum(Num: Integer): Pointer;

  procedure SetOpenMode(AOpenMode : TOpenModes);

  // 2016.03.02 - BeaR: ������ ������� �������� � �������� ����� � ������ ���� � ����� FFileName �� ������, � ������, ���� �� ��� ����� �����������
  procedure ValidateFileName;
 public
   // 2015.12.22 - MysticCoder: ���� True, �� ��� ���������� ������������ ������ ������� WriteRecord � ��� ��������, ������ ������ �� ��������, ����� ������ ����������� � �����
  DoForceExistRecordIndexOnReWrite : boolean;
  //=========================================================================
  // PUBLIC Properties
  //=========================================================================

  // 2015.01.15 BeaR: ������� ���� ��������� �� �������� ��� �������� �����. �� ��������� false
  property DoCompareCheckSumm : boolean read FDoCompareCheckSumm write FDoCompareCheckSumm;

  property UpdatedOnce: Boolean read FUpdatedOnce;

  property FileSize: Cardinal read FFileSize;
  property Password: Pointer read GetPassword;
  property PassSize: Integer read FPassSize;

  property RecordCount: Integer read GetRecordCount;
  property RecordKey[Num: Integer]: string read GetRecordKey;
  property RecordPhysSize[Num: Integer]: Integer read GetRecordPhysSize;
  property RecordOrigSize[Num: Integer]: Integer read GetRecordOrigSize;
  property RecordNum[Key: string]: Integer read GetRecordNum;
  property RecordType[Num: Integer]: Integer read GetRecordType;
  property RecordDate[Num: Integer]: TDateTime read GetRecordDate;
  property RecordChecksum[Num: Integer]: Pointer read GetRecordChecksum;

  property RecordCompressionType[Num: Integer]: Word read GetRecordCompressionType;
  property RecordForceCompressionType[Num: Integer]: Boolean read GetRecordForceCompressionType;
  property RecordColorFormat[Num: Integer]: Word read GetRecordColorFormat;
  property RecordPatternCount[Num: Integer]: Word read GetRecordPatternCount;
  property RecordPatternWidth[Num: Integer]: Word read GetRecordPatternWidth;
  property RecordPatternHeight[Num: Integer]: Word read GetRecordPatternHeight;

  //=========================================================================
  // PUBLIC Methods
  //=========================================================================

  // updates the password used to decode records
  procedure SetPassword(MemAddr: Pointer; Size: Integer);

  // removes the stored password and replaces it with zeros
  procedure BurnPassword();

  // 2015.02.06 BeaR: ASourceIsCompressed - ������� � true, ���� ���������� ��� Source ��� ���� ������� ����
  // AUncompressedSize �������� ������ � ������ ���� ASourceIsCompressed = true - ��� ������ �������� ������
  // writes the specific record to ASDb archive
  function WriteRecord(const Key: string; Source: Pointer;
   SourceSize: Cardinal; RecordType: Integer; AForceDateTime : TDateTime = 0; ASourceIsCompressed : boolean = false; AUncompressedSize : cardinal = 0;
   ACompressionType : Word = rctMax; AForceCompressionType : boolean = false; AColorFormat : Word = 0; APatternCount : Word = 0; APatternWidth : Word = 0; APatternHeight : Word = 0): Boolean;

  // writes the entire stream to ASDb archive
  function WriteStream(const Key: string; Stream: TStream;
   RecordType: Integer; AForceDateTime : TDateTime = 0; ASourceIsCompressed : boolean = false; AUncompressedSize : cardinal = 0;
   ACompressionType : Word = rctMax; AForceCompressionType : boolean = false; AColorFormat : Word = 0; APatternCount : Word = 0; APatternWidth : Word = 0; APatternHeight : Word = 0): Boolean;

  function WriteString(const Key, Text: string;
   RecordType: Integer): Boolean;

  // 2015.01.15 BeaR: ��������� ������ ReadCompressedRecord � DecompressRecord
  // ��� �������� ����� - ������ � memoryholder, ������ � ����

  // ������ ������ ������ (�� ������ ����������� security)
  // ������ ����� � ��������� ������. ������ � ��� ������ ���� �������� � ���� ����� ��� ������ PhysSize �������� ������
  function ReadCompressedRecord(const AKey: string; AData : pointer) : boolean; overload;

  // ���������� �����������, �� ��������� ��� ���������� ����� ��������� ������
  function ReadCompressedRecord(out AStreamReadTime : cardinal; const AKey: string; AData : pointer) : boolean; overload;


  // ������ ������. �������� ������ � AData
  function ReadCompressedRecord(const AKey: string; out AData : pointer; out ADataSize : cardinal) : boolean; overload;

  // ������ ������. �������� ������ � AData. ����� ���������� ��������� ����� ��������� ������
  function ReadCompressedRecord(const AKey: string; out AData : pointer; out ADataSize : cardinal; out AStreamReadTime : cardinal) : boolean; overload;


  {function _Debug_CachedIndex_ReadCompressedRecord(out AStreamReadTime : extended; const AKey: string; AData : pointer) : boolean; overload;
  function _Debug_CachedIndex_ReadCompressedRecord(const AKey: string; out AData : pointer; out ADataSize : cardinal; out AStreamReadTime : extended) : boolean; overload;}

  // ��������� ���������� ����� ReadCompressedRecord() ������. ���������� ����������� ������ � ADecompressedData (������ DecompressedDataSize ����).
  // �� ����� ����������� ����� FreeMem() ���������� ReadRecord()
  // ����� � ������� �� ReadRecord() ����������� ����� ������� �� �����������
  class function DecompressRecord(ACompressedData : pointer; ACompressedDataSize: cardinal; ADecompressedDataSize: cardinal; out ADecompressedData: pointer) : boolean;

  // ���������� DecompressRecord, �� �������� ������
  class function CompressRecord(ADecompressedData : pointer; ADecompressedDataSize: cardinal; out ACompressedData: pointer; out ACompressedDataSize: cardinal; ACompressionLevel : TCompressionLevel = clHighest) : boolean;

  // reads the specified record from ASDb archive
  // NOTE: this method allocates memory which needs to be freed by FreeMem
  function ReadRecord(const Key: string; out Data: Pointer;
   out DataSize: Cardinal): Boolean;

  {$IFDEF USE_MEMORYHOLDER}
  // 2015.01.27: �� ������ ReadRecord. ������ �� �� ����� �� ������� ���������� ����� ������ ������ �� � ����� � � Interprocessed Data catch (MemoryHolder.exe)
  function ReadRecordUsingInterprocessedDataCatch(const Key: string; out Data: Pointer;
   out DataSize: Cardinal): Boolean;
  {$ENDIF}

  // reads the record and stores it in the stream
  function ReadStream(const Key: string; Stream: TStream): Boolean;

  // reads ASDb record contents as a text string
  function ReadString(const Key: string; out Text: string): Boolean;

  // removes the record from archive
  function RemoveRecord(const Key: string): Boolean;

  // changes the key of the record without physically moving it
  function RenameRecord(const Key, NewKey: string): Boolean;

  // switches the positions of two records
  function SwitchRecords(Index1, Index2: Integer): Boolean;

  // sorts the records by type not affecting order of items with the same type
  function SortRecords(): Boolean;

  // 2015.02.17 BeaR: ����� ��������� ���������� � force compression �����. ���� ���������� ���������� - ����������������� ������
  function ChangeCompression(const AKey : string; ACompressionType : word; AForceCompressionType : boolean) : boolean;

  constructor Create(AOwner: TComponent); override;

  // updates the list of ASDb records
  function Update(): Boolean;

  // updates the record list only once
  function UpdateOnce(): Boolean;

  function ReadImageHeader(const Key : string; out AHeader : TAsphyreImageHeader) : boolean;
  
 published
  // The name of the archive
  property FileName: string read FFileName write SetFileName;

  // open mode (e.g. WriteBuffer-only)
  property OpenMode: TOpenModes read FOpenMode write SetOpenMode;
 end;

//---------------------------------------------------------------------------
// 2014.05.27 MysticCoder: ��������� bmp ���� � ASDB
// ����������� � http://afterwarp.net/forum/thread1594.html

procedure SaveBMPToAsdb(ABMPFileName : String; AAsdb : TASDb; Key : String); overload;
procedure SaveBMPToAsdb(ABMP : Graphics.TBitmap; AAsdb : TASDb; Key : String); overload;

{var
  UASDB_CurrentAccumulatedTimeForHeadersReading : cardinal;
  UASDB_CurrentAccumulatedTimeForStreamReading : cardinal;
  UASDB_CurrentAccumulatedTimeForDecompressing : cardinal;
  UASDB_CurrentStreamsReadedCount : integer;
  UASDB_CurrentStreamsReadedAccumulatedSize : cardinal;     }

implementation

uses
  pxfm, {UniTypes,} ImageFx,
 {$IFDEF UseImageLoadBuffer}
  UniMemoryBuffer,
 {$ENDIF}
 {$IFDEF USE_MEMORYHOLDER}
 UniMemoryHolderManager,
 {$ENDIF}
 PsAPI;

  {$IFDEF UseASDBCachedInfo}
  var
  UASDbCachedInfo : TASDbCachedInfo;
  {$ENDIF}

//  UASDB_StatsisticsCriticalSection : TCriticalSection;

// 2013.01.21 - Corris: ����������� �������� � DeMonk (��� ������� ��������� � ����� ������� - ����� ������� ������, ����� Result �����)
// 2013.01.18 - Corris: ���������� ��������� ������� ������ ������ �� ����������� ����� (��� ��� ������, ���� ��� �������� �������� ������)
function RoundUpToPowerOf2(AValue : real) : cardinal;
var
  xValue_Int, xValue_Int_Start : cardinal;
begin
  xValue_Int_Start := Round(AValue); // ��������� � �������
  xValue_Int := xValue_Int_Start; // ��������� �� ������
  // ������ ����� ����� ��������� ������ (����� ���������� ������� ������� ������ = +1 ������)

  if xValue_Int = 0 then
    begin
      Result := 1;
      Exit;
    end;
  Result := 1;
  xValue_Int := xValue_Int shr 1;
  while xValue_Int <> 0 do
    begin
      xValue_Int := xValue_Int shr 1;
      Result := Result shl 1;
    end;
  if Result <> xValue_Int_Start then
    Result := Result shl 1;

end;

// 2014.05.27 MysticCoder: ��������� bmp ���� � ASDB
// ����������� � http://afterwarp.net/forum/thread1594.html
procedure SaveBMPToAsdb(ABMPFileName : String; AAsdb : TASDb; Key : String);
var
  xBmp : Graphics.TBitmap;
  xDestBmp : Graphics.TBitmap;
  PxFm : TPxFm;
  pSize : TPoint;
  IsMasked : boolean;
  Tolerance : integer;
  MaskColor : integer;
  Stream : TMemoryStream;
begin
  xBmp := TBitmap.Create;
  xBmp.LoadFromFile(ABMPFileName);

  PxFm.Format := COLOR_A8R8G8B8;

// retreive Texture Size from edit boxes
  PxFm.TextureWidth := RoundUpToPowerOf2(xBmp.Width);
  PxFm.TextureHeight := RoundUpToPowerOf2(xBmp.Height);

// retreive Pattern Size from edit boxes
  PxFm.PatternWidth := xBmp.Width;
  PxFm.PatternHeight:= xBmp.Height;

// this variable is used for better readability only
  pSize := Point(PxFm.PatternWidth, PxFm.PatternHeight);

// this size can be smaller than pattern size to add padding
  PxFm.VisibleWidth := PxFm.PatternWidth;
  PxFm.VisibleHeight:= PxFm.PatternHeight;

// retreive mask color and tolerance
  IsMasked := False;
  MaskColor := 0;
  Tolerance := 0;

  PxFm.PatternCount:= (xBmp.Width div pSize.X) * (xBmp.Height div pSize.Y);

// create destination bitmap
  xDestBmp := TBitmap.Create();
  TileBitmap(xDestBmp, xBmp, Point(PxFm.TextureWidth, PxFm.TextureHeight), pSize, pSize, IsMasked, MaskColor, Tolerance);

  // we don't need source image anymore
  xBmp.Free();

  // calculate number of textures created
  PxFm.TextureCount:= xDestBmp.Height div PxFm.TextureHeight;

  // create auxiliary stream to write PxFm-formatted image data
  Stream:= TMemoryStream.Create();
  WriteBitmapPxFm(Stream, xDestBmp, PxFm);
  // we don't need destination image anymore
  xDestBmp.Free();

  // position to the beginning of our stream
  Stream.Seek(0, soFromBeginning);

  // write PxFm-formatted image data to ASDb
  if (AASDb.WriteStream(Key, Stream, recGraphics, 0, false, 0, rctMax, true, Word(PxFm.Format), Word(PxFm.PatternCount), Word(PxFm.PatternWidth), Word(PxFm.PatternHeight))) then

  Stream.Free();
end;

procedure SaveBMPToAsdb(ABMP : Graphics.TBitmap; AAsdb : TASDb; Key : String);
var
  xBmp : Graphics.TBitmap;
  xDestBmp : Graphics.TBitmap;
  PxFm : TPxFm;
  pSize : TPoint;
  IsMasked : boolean;
  Tolerance : integer;
  MaskColor : integer;
  Stream : TMemoryStream;
begin
  xBmp := ABMP;
//  xBmp := TBitmap.Create;
//  xBmp.LoadFromStream(ABMP);

  PxFm.Format := COLOR_A8R8G8B8;

// retreive Texture Size from edit boxes
  PxFm.TextureWidth := RoundUpToPowerOf2(xBmp.Width);
  PxFm.TextureHeight := RoundUpToPowerOf2(xBmp.Height);

// retreive Pattern Size from edit boxes
  PxFm.PatternWidth := xBmp.Width;
  PxFm.PatternHeight:= xBmp.Height;

// this variable is used for better readability only
  pSize := Point(PxFm.PatternWidth, PxFm.PatternHeight);

// this size can be smaller than pattern size to add padding
  PxFm.VisibleWidth := PxFm.PatternWidth;
  PxFm.VisibleHeight:= PxFm.PatternHeight;

// retreive mask color and tolerance
  IsMasked := False;
  MaskColor := 0;
  Tolerance := 0;

  PxFm.PatternCount:= (xBmp.Width div pSize.X) * (xBmp.Height div pSize.Y);

// create destination bitmap
  xDestBmp := TBitmap.Create();
  TileBitmap(xDestBmp, xBmp, Point(PxFm.TextureWidth, PxFm.TextureHeight), pSize, pSize, IsMasked, MaskColor, Tolerance);

  // we don't need source image anymore
//  xBmp.Free();

  // calculate number of textures created
  PxFm.TextureCount:= xDestBmp.Height div PxFm.TextureHeight;

  // create auxiliary stream to write PxFm-formatted image data
  Stream:= TMemoryStream.Create();
  WriteBitmapPxFm(Stream, xDestBmp, PxFm);
  // we don't need destination image anymore
  xDestBmp.Free();

  // position to the beginning of our stream
  Stream.Seek(0, soFromBeginning);

  // write PxFm-formatted image data to ASDb
  if (AASDb.WriteStream(Key, Stream, recGraphics, 0, false, 0, rctMax, true, Word(PxFm.Format), Word(PxFm.PatternCount), Word(PxFm.PatternWidth), Word(PxFm.PatternHeight))) then

  Stream.Free();
end;

//---------------------------------------------------------------------------
const
 // A record name returned when invalid index is specified
 invRecordName = '[invalid-record-#]';

 // When using compression, a temporary buffer is used to store the final
 // output. Under certain circumstances, the output data size is bigger than
 // the original. For these cases, output buffer is created slightly bigger
 // than the original. The additional percentage added is specified below.
 BufferGrow    = 5; // default: 5 (in %)

 // For the same purpose as BufferGrow, this value is simply added to the
 // buffer size previously increased by BufferGrow (for very short buffers).
 BufferGrowAdd = 256; // default: 256

 // In original record position, this offset determines where record data
 // is allocated. This is used for ReadRecord method to get directly to
 // record data. Also used for removing records.
 DataOffset    = 44;


{$IFDEF UseASDBCachedInfo}
constructor TASDbCachedInfo.Create;
begin
  Items := TUniList.Create;
  (Items as TUniList).DoDestroyElements := True;
  CriticalSection := TCriticalSection.Create;
end;

destructor TASDbCachedInfo.Destroy;
//var
//  i : integer;
begin
(*
  // 2016.02.26 - MysticCoder: �������� DoDestroyElements
  for i := 0 to Items.Count - 1 do
    begin
      if Items[i] <> nil then
        TASDbCachedInfo_Item(Items[i]).Destroy;
    end; *)
  Destnil(CriticalSection);
  Destnil(Items);
end;

procedure TASDbCachedInfo.AddRecordsInfo(ASDbPath : string; var AInfoArrayToGetFrom : TRecordInfoArray);
var
  xNewItem : TASDbCachedInfo_Item;
begin
  CriticalSection.Enter;

  xNewItem := TASDbCachedInfo_Item.Create;
  xNewItem.ASDbPath := UpperCase(ASDbPath);
 // xNewItem.RecordsInfo := AInfoArrayToGetFrom;
  SetLength(xNewItem.RecordsInfo, Length(AInfoArrayToGetFrom));
  xNewItem.RecordsInfo := Copy(AInfoArrayToGetFrom, 0, Length(AInfoArrayToGetFrom));

  Items.Add(xNewItem);

  CriticalSection.Leave;
end;

function TASDbCachedInfo.GetRecordsInfo(ASDbPath : string; var AInfoArrayToSetTo : TRecordInfoArray) : boolean;
var
  i : integer;
  xUpperPath : string;
  xCurrentItem : TASDbCachedInfo_Item;
begin
  xUpperPath := UpperCase(ASDbPath);

  CriticalSection.Enter;

  for i := 0 to Items.Count - 1 do
    begin
      xCurrentItem := TASDbCachedInfo_Item(Items[i]);
      if xUpperPath = xCurrentItem.ASDbPath then
        begin
            SetLength(AInfoArrayToSetTo, Length(xCurrentItem.RecordsInfo));
            AInfoArrayToSetTo := Copy(xCurrentItem.RecordsInfo, 0, Length(xCurrentItem.RecordsInfo));

            CriticalSection.Leave;
            Result := true;
            Exit;
        end;
    end;

  CriticalSection.Leave;
  Result := false;
end;

procedure TASDbCachedInfo.ClearRecordsInfo(ASDbPath : string);
var
  i : integer;
  xUpperPath : string;
  xCurrentItem : TASDbCachedInfo_Item;
begin
  xUpperPath := UpperCase(ASDbPath);

  CriticalSection.Enter;

  for i := 0 to Items.Count - 1 do
    begin
      xCurrentItem := TASDbCachedInfo_Item(Items[i]);
      if xUpperPath = xCurrentItem.ASDbPath then
        begin
            Items.Delete(i);

            CriticalSection.Leave;
            Exit;
        end;
    end;

  CriticalSection.Leave;
end;

{$ENDIF}

//---------------------------------------------------------------------------
constructor TASDb.Create(AOwner: TComponent);
begin
 inherited;

 FDoCompareCheckSumm := false;

 FUpdatedOnce:= False;
 FFileSize:= 0;
 FFileName:= '';
 FOpenMode:= opReadOnly;
 FPassSize:= 0;

 SetLength(FRecords, 0);
 FillChar(ASDbHeader, SizeOf(TASDbHeader), 0);
 FillChar(PassBlock, SizeOf(PassBlock), 0);
end;

//---------------------------------------------------------------------------
function TASDb.GetPassword(): Pointer;
begin
 Result:= @PassBlock;
end;

//---------------------------------------------------------------------------
procedure TASDb.BurnPassword();
begin
 FillChar(PassBlock, SizeOf(PassBlock), 0);
 FPassSize:= 0;
end;

//---------------------------------------------------------------------------
procedure TASDb.SetPassword(MemAddr: Pointer; Size: Integer);
begin
 // erase previously stored password
 BurnPassword();

 // validate key sizes
 if (Size < 1) then Exit;
 if (Size > 56) then Size:= 56;

 // store a new key
 Move(MemAddr^, PassBlock, Size);
 FPassSize:= Size;
end;

//---------------------------------------------------------------------------
function TASDb.GetRecordCount(): Integer;
begin
 Result:= Length(FRecords);
end;

//---------------------------------------------------------------------------
function TASDb.GetRecordPhysSize(Num: Integer): Integer;
begin
 if (Num >= 0)and(Num < Length(FRecords)) then
  begin
   Result:= FRecords[Num].PhysSize;
  end else Result:= 0;
end;

//---------------------------------------------------------------------------
function TASDb.GetRecordOrigSize(Num: Integer): Integer;
begin
 if (Num >= 0)and(Num < Length(FRecords)) then
  begin
   Result:= FRecords[Num].OrigSize;
  end else Result:= 0;
end;

//---------------------------------------------------------------------------
function TASDb.GetRecordType(Num: Integer): Integer;
begin
 if (Num >= 0)and(Num < Length(FRecords)) then
  begin
   Result:= FRecords[Num].RecordType;
  end else Result:= 0;
end;

//---------------------------------------------------------------------------
function TASDb.GetRecordKey(Num: Integer): string;
begin
 if (Num >= 0)and(Num < Length(FRecords)) then
  begin
   Result:= FRecords[Num].Key;
  end else Result:= invRecordName;
end;

//---------------------------------------------------------------------------
function TASDb.GetRecordDate(Num: Integer): TDateTime;
begin
 if (Num >= 0)and(Num < Length(FRecords)) then
  begin
   Result:= FRecords[Num].DateTime;
  end else Result:= Now();
end;

//---------------------------------------------------------------------------

function TASDb.GetRecordCompressionType(Num: Integer): Word;
begin
 if (Num >= 0)and(Num < Length(FRecords)) then
  begin
   Result:= FRecords[Num].CompressionType;
  end else Result := rctOldNoSecure;
end;

function TASDb.GetRecordForceCompressionType(Num: Integer): Boolean;
begin
 if (Num >= 0)and(Num < Length(FRecords)) then
  begin
   Result:= FRecords[Num].ForceCompressionType;
  end else Result := false;
end;

function TASDb.GetRecordColorFormat(Num: Integer): Word;
begin
 if (Num >= 0)and(Num < Length(FRecords)) then
  begin
   Result:= FRecords[Num].ColorFormat;
  end else Result:= 0;
end;

function TASDb.GetRecordPatternCount(Num: Integer): Word;
begin
 if (Num >= 0)and(Num < Length(FRecords)) then
  begin
   Result:= FRecords[Num].PatternCount;
  end else Result:= 0;
end;

function TASDb.GetRecordPatternWidth(Num: Integer): Word;
begin
 if (Num >= 0)and(Num < Length(FRecords)) then
  begin
   Result:= FRecords[Num].PatternWidth;
  end else Result:= 0;
end;

function TASDb.GetRecordPatternHeight(Num: Integer): Word;
begin
 if (Num >= 0)and(Num < Length(FRecords)) then
  begin
   Result:= FRecords[Num].PatternHeight;
  end else Result:= 0;
end;

//---------------------------------------------------------------------------
function TASDb.GetRecordChecksum(Num: Integer): Pointer;
begin
 if (Num >= 0)and(Num < Length(FRecords)) then
  begin
   Result:= @FRecords[Num].Checksum;
  end else Result:= nil;
end;

procedure TASDb.SetOpenMode(AOpenMode : TOpenModes);
begin
  FOpenMode := AOpenMode;

  {$IFDEF UseASDBCachedInfo}
  // ���� ������ �� ����� ������ - ������� ���, ��� ���� ��� ����� �� �� ��� ��������������
  if (FOpenMode <> opReadOnly) then
    UASDbCachedInfo.ClearRecordsInfo(FFileName);
  {$ENDIF}
end;

procedure TASDb.ValidateFileName;
var
  xSearchResult : TSearchRec;
begin
  if FindFirst(FFileName, faAnyFile, xSearchResult) = 0 then
    begin
      Delete(FFileName, Length(FFileName) - Length(xSearchResult.Name) + 1, Length(xSearchResult.Name));
      FFileName := FFileName + xSearchResult.Name;
      FindClose(xSearchResult);
    end;
end;

//---------------------------------------------------------------------------
procedure TASDb.SetFileName(const Value: string);
begin
 FFileName:= Value;

 // 2016.03.02 - BeaR: ���� ������ ��� ����� � ������������ ��������, �� ������ ��� ��������� .asdb-����� � ���� ������� ������� �� �������� ������������
 // ��� �������� ������ ����� � ��� ��� ���� �� ���������������� � ��������� ����� �� ��������� FFileName � �������� ���������. ��� ���� ����� ��������� ���,
 // � ����� enumerate �� ����� ���������� ������ ���� � � ������ ���������� ������ �� �����, �� � ������ �������� - ����� � FFileName ������� � ����� �����
 // (�������� ������ ����� � ������ �����, ��������� ���� �� ���������)
 ValidateFileName;

 FUpdatedOnce:= False;
end;

//---------------------------------------------------------------------------
function TASDb.GetRecordNum(Key: string): Integer;
var
 i: Integer;
begin
 Key:= LowerCase(Key);

 for i:= 0 to Length(FRecords) - 1 do
  if (LowerCase(FRecords[i].Key) = Key) then
   begin
    Result:= i;
    Exit;
   end;

 Result:= -1;
end;

//---------------------------------------------------------------------------
function TASDb.CreateEmtpyFile(): Boolean;
var
 fs: TStream;
begin
 // prepare empty header
 FillChar(ASDbHeader, SizeOf(TASDbHeader), 0);
 ASDbHeader.Signature:= ASDbSignature;
 ASDbHeader.RecordCount:= 0;
 // offset to non-existant table
 ASDbHeader.TableOffset:= SizeOf(TASDbHeader);

 // create file stream
 try
  fs:= TFileStream.Create(FFileName, fmCreate or fmShareExclusive);
 except
  Result:= False;
  Exit;
 end;

 // write header
 Result:= True;
 try
  fs.WriteBuffer(ASDbHeader, SizeOf(ASDbHeader));
 except
  Result:= False;
 end;

 // free file stream
 fs.Free();

 // file size
 FFileSize:= SizeOf(ASDbHeader);

 // assume no records exist
 SetLength(FRecords, 0);
end;

//---------------------------------------------------------------------------
function TASDb.ReadASDbHeader(Stream: TStream; ASDbHeader: PASDbHeader): Boolean;
begin
 if (Stream = nil) then
  begin
   Result:= False;
   Exit;
  end;

 Result:= True;
 try
  // read ASDb Header
  Stream.Seek(0, soFromBeginning);
  Stream.ReadBuffer(ASDbHeader^, SizeOf(TASDbHeader));
 except
  Result:= False;
 end;
end;

//---------------------------------------------------------------------------
function TASDb.ReadASDbInfo(Stream: TStream): Boolean;
var
 i: Integer;
 NoStream: Boolean;
 xFlags : Word;
begin
  {$IFDEF UseASDBCachedInfo}
  // ����������� � ������ � ����.
  if (OpenMode = opReadOnly) and UASDbCachedInfo.GetRecordsInfo(FFileName, FRecords) then
    begin
      FRecordsReadOnce := true;
      Result := true;
      Exit;
    end;

  // ���� ����� ���� ������� � ���� ��� (���� ��������� ��� �� ����� � �������� � ���

  {$ENDIF}

 // release records
 SetLength(FRecords, 0);
 NoStream:= (Stream = nil);

 if (NoStream) then
  begin
   // open the specified file
   try
    Stream:= TFileStream.Create(FFileName, fmOpenRead or fmShareDenyWrite);
   except
    Result:= False;
    Exit;
   end;
  end;

 // read & validate ASDbHeader
 Result:= ReadASDbHeader(Stream, @ASDbHeader);
 if (not Result) then
  begin
   if (NoStream) then Stream.Free();
   Exit;
  end;

 // retreive file size
 FFileSize:= Stream.Size;

 // seek record table in archive
 Stream.Seek(ASDbHeader.TableOffset, soBeginning);

 // specify record count
 SetLength(FRecords, ASDbHeader.RecordCount);

 // read record names and positions
 try
  for i:= 0 to Length(FRecords) - 1 do
   begin
    // read record name
    FRecords[i].Key:= stReadString(Stream);
    FRecords[i].Offset:= stReadLongword(Stream);
   end;
 except
  Result:= False;
 end;

 // check for Read errors
 if (not Result) then
  begin
   // no records are saved on error
   SetLength(FRecords, 0);
   if (NoStream) then Stream.Free();
   Exit;
  end;

 // check if any records exist in archive
 if (ASDbHeader.RecordCount < 1) then
  begin
   if (NoStream) then Stream.Free();
   Exit;
  end;

 // read record detailed information
 try
  for i:= 0 to Length(FRecords) - 1 do
   begin
    // seek record's position
    Stream.Seek(FRecords[i].Offset, soBeginning);

    // record type
    FRecords[i].RecordType:= stReadWord(Stream);

    // basic info
    FRecords[i].OrigSize:= stReadLongword(Stream);
    FRecords[i].PhysSize:= stReadLongword(Stream);
    FRecordS[i].DateTime:= stReadDouble(Stream);

    // MD5 message-digest of record's contents
    Stream.ReadBuffer(FRecords[i].Checksum, SizeOf(FRecords[i].Checksum));

    // ������ �������:
    // security information
    //FRecords[i].Secure:= Boolean(stReadWord(Stream));
    //Stream.ReadBuffer(FRecords[i].InitBlock, SizeOf(FRecords[i].InitBlock));

    // ��������� ��� ���������� + force ���� / ������ ������� secure �����
    xFlags := stReadWord(Stream);
    
    FRecords[i].CompressionType := (xFlags and $7FFF); // �������� ��������� ���
    FRecords[i].ForceCompressionType := (xFlags and $8000) = $8000; //< ������ true ���� ���� ��������� ���

    FRecords[i].ColorFormat := stReadWord(Stream);
    FRecords[i].PatternCount := stReadWord(Stream);
    FRecords[i].PatternWidth := stReadWord(Stream);
    FRecords[i].PatternHeight := stReadWord(Stream);
   end; // for
 except
  Result:= False;
 end;

 // release stream's memory
 if (NoStream) then Stream.Free();

 {$IFDEF UseASDBCachedInfo}
 if (OpenMode = opReadOnly) and Result then
  begin
    // ���� ����������� ����, �� ����� �������� ������ � ���.
    UASDbCachedInfo.AddRecordsInfo(FFileName, FRecords);
    FRecordsReadOnce := true;
  end;
 {$ENDIF}
end;

//---------------------------------------------------------------------------
function TASDb.Update(): Boolean;
begin
 Result:= True;
 
 // act depending of opening mode
 case FOpenMode of
  // create new file
  opOverwrite:
   Result:= CreateEmtpyFile();

  // open file for reading
  opReadOnly:
   Result:= ReadASDbInfo(nil);

  // open file for update
  opUpdate:
   begin
    if (FileExists(FFileName)) then Result:= ReadASDbInfo(nil)
     else Result:= CreateEmtpyFile();
   end;
 end;

 FUpdatedOnce:= Result;
end;

//---------------------------------------------------------------------------
function TASDb.UpdateOnce(): Boolean;
begin
 Result:= FUpdatedOnce;
 if (not Result) then Result:= Update();
end;

//---------------------------------------------------------------------------

function TASDb.ReadImageHeader(const Key : string; out AHeader : TAsphyreImageHeader) : boolean;
var
  Stream: TStream;
begin
  FillMemory(@AHeader, SizeOf(AHeader), 0);

  {$IFDEF UseImageLoadBuffer}
  Stream := TMemoryBuffer_Stream.Create(UniMemoryBuffer.u_ImagesMemory);
  {$ELSE}
  Stream := Classes.TMemoryStream.Create();
  {$ENDIF}
  try
    // (1) Make sure ASDb is up-to-date.
    Result := UpdateOnce();
    if not Result then 
      Exit;

    // (2) Read the requested record as stream.
    try
      Result := ReadStream(Key, Stream);
    except
      on E:Exception do
        raise ExceptClass(E.ClassType).Create('TASDb.ReadImageHeader: Key=' + Key + ' ASDb=' + FileName + ' : ReadStream exception  =>  ' + E.Message);
    end;
    
    if not Result then
      Exit;

    // (4) Load texture information.
    try
      Stream.Seek(0, soFromBeginning);
      Stream.ReadBuffer(AHeader, SizeOf(AHeader));
    except
      FillMemory(@AHeader, SizeOf(AHeader), 0);
      Result := False;
    end;

  finally
    // (5) Release the stream.
    Stream.Free();
  end;
end;  

//---------------------------------------------------------------------------
class function TASDb.CompressData(Source: Pointer; SourceSize: Cardinal;
 out Data: Pointer; out DataSize: Cardinal; ACompression : TCompressionLevel = clHighest): Boolean;
var
 //CodeBuf   : Pointer;
 //BufferSize: Cardinal;

 xOutSize : integer;
begin
 (*Result:= True;

 // guaranteed buffer size
 BufferSize:= Ceil((SourceSize * (100 + BufferGrow)) / 100) + BufferGrowAdd;

 // allocate encoding buffer
 GetMem(CodeBuf, BufferSize);

 // inflate the buffer
 DataSize:= AsphyreData.CompressData(Source, CodeBuf, SourceSize, BufferSize,
  clHighest);
 if (DataSize = 0) then
  begin
   FreeMem(CodeBuf);
   Result:= False;
   Exit;
  end;

 // allocate real data container
 GetMem(Data, DataSize);

 // copy the compressed data
 Move(CodeBuf^, Data^, DataSize);

 // release encoding buffer
 FreeMem(CodeBuf);*)

  // 2015.02.06 BeaR: ������ ������ ���������. �������� �����-�� ���� ������������� �������� � �������� ��������� ���������� �� ������ ������������� �������
  // (� ��������� ������� ������������ ��������) �������� ZCompress ��� �����������.

  AsphyreData.CompressDataDirect(Source, SourceSize, Data, xOutSize, ACompression);
  DataSize := xOutSize;

  Result := Data <> nil;
end;


//---------------------------------------------------------------------------
function GetOwnMemorySize: String;
var
  pmc: PPROCESS_MEMORY_COUNTERS;
  cb: Integer;
begin
  cb := SizeOf(_PROCESS_MEMORY_COUNTERS);
  GetMem(pmc, cb);
  pmc.cb := cb;
  if GetProcessMemoryInfo(GetCurrentProcess(), pmc, cb) then
    Result := IntToStr(pmc.WorkingSetSize) + ' Bytes'
  else
    Result := 'Unable to retrieve memory usage structure';
  FreeMem(pmc);
end;

class function TASDb.DecompressData(Source: Pointer; SourceSize: Cardinal;
 out Data: Pointer; DataSize: Longword): Boolean;
const
  x_DoNotUseExtDestPointerInDecompress = True;
var
 OutSize: Integer;


  xDebugPointer: Pointer;
  xDebugDataSize: Integer;
  xDebug_Ok: Boolean;
begin
 Result:= True;

 // allocate output buffer
 try

 if not x_DoNotUseExtDestPointerInDecompress then
  begin
   {$IFDEF UseImageLoadBuffer}
    Data := u_ImagesMemory.GetMem_Cache_TwoPiece(DataSize, nil, 'Unpack data: ASDB.ReadData');
   {$ELSE}
    GetMem(Data, DataSize);
   {$ENDIF}
  end
  else
    Data := nil;

 except
  on E:EOutOfMemory do
    begin
      xDebugDataSize := DataSize;
      xDebug_Ok := False;
      xDebugPointer := nil;
      repeat
        try
          GetMem(xDebugPointer, xDebugDataSize);
          xDebug_Ok := True;
        except
          dec(xDebugDataSize, Max(xDebugDataSize div 10, 4096));
        end;
      until xDebug_Ok or (xDebugDataSize <= 2048);
      if xDebug_Ok then
        FreeMem(xDebugPointer, xDebugDataSize);
      if xDebug_Ok then
        raise ExceptClass(E.ClassType).Create('Except in TASDb.DecompressData:' + E.Message + ' OwnMemorySize=' + GetOwnMemorySize + ' AllocSize=' + IntToStr(DataSize) + 'b. Second alloc mem: ' + intToStr(xDebugDataSize) + 'b. ')
      else
        raise ExceptClass(E.ClassType).Create('Except in TASDb.DecompressData:' + E.Message + ' OwnMemorySize=' + GetOwnMemorySize + ' AllocSize=' + IntToStr(DataSize) + 'b. Second alloc mem - error. ');
    end;
  end;

 // decompress the data stream
 try
  // ����������� ��� ���� "�� ������������ ���������� ���������", ��� ���� ������ ������ �� ��� ��������� ����, � �� �������� ���������������� (����������). ����� (�����) ���������� ������. 
  OutSize:= AsphyreData.DecompressData(Source, Data, SourceSize, DataSize, x_DoNotUseExtDestPointerInDecompress);
 except
   {$IFDEF UseImageLoadBuffer}
    u_ImagesMemory.FreeMem_Cache_TwoPiece(Data, nil);
   {$ELSE}
    FreeMem(Data);
   {$ENDIF}
   Data := nil;
   Raise;
 end;

 if (OutSize = 0)or(Int64(OutSize) <> DataSize) then
  begin
   {$IFDEF UseImageLoadBuffer}
    u_ImagesMemory.FreeMem_Cache_TwoPiece(Data, nil);
   {$ELSE}
    FreeMem(Data);
   {$ENDIF}
   Data:= nil;
   Result:= False;
  end;
end;

//---------------------------------------------------------------------------
function TASDb.WriteRecordTable(): Boolean;
var
 Stream: TFileStream;
 i: Integer;
begin
 Result:= True;

 // (1) OPEN THE ARCHIVE for *writing*
 try
  Stream:= TFileStream.Create(FFileName, fmOpenWrite or fmShareExclusive);
 except
  Result:= False;
  Exit;
 end;

 try
  // (2) go to the position of record table
  Stream.Seek(ASDbHeader.TableOffset, soBeginning);

  // (3) flush the record table
  for i:= 0 to Length(FRecords) - 1 do
   begin
    stWriteString(Stream, FRecords[i].Key);
    stWriteLongword(Stream, FRecords[i].Offset);
   end;
 except
  Result:= False;
 end;

 // (4) release the file stream
 Stream.Free();
end;

//---------------------------------------------------------------------------
function TASDb.WriteRecord(const Key: string; Source: Pointer;
 SourceSize: Cardinal; RecordType: Integer; AForceDateTime : TDateTime = 0; ASourceIsCompressed : boolean = false; AUncompressedSize : cardinal = 0;
 ACompressionType : Word = rctMax; AForceCompressionType : boolean = false; AColorFormat : Word = 0; APatternCount : Word = 0; APatternWidth : Word = 0; APatternHeight : Word = 0): Boolean;
var
 Data: Pointer;
 DataSize: Cardinal;
 i, NewIndex: Integer;
 Stream: TStream;
 RecordOffset: Cardinal;
 Subkeys: array[0..1] of Longword;
 Checksum: array[0..3] of Longword;
 CurDate: TDateTime;

 xCompression : TCompressionLevel;

 xFlags : word;

 xDoCompress : boolean;

 xDecompressedBuf : pointer;
 xIndex : integer;
begin
 Result := False;
 if AForceDateTime = 0 then // 2014.09.15 - MysticCoder: ���� ������� �������� ������ �����\����, �� ��� �� ���������(������)
   CurDate:= Now()
 else
  CurDate := AForceDateTime;

 // (1) verify open mode
 if (FOpenMode = opReadOnly) then Exit;

 xIndex := GetRecordNum(Key);
 // (2) if the record exists, remove it
 if (xIndex <> -1) then RemoveRecord(Key);

 xDoCompress := (not ASourceIsCompressed) and (ACompressionType <> rctNoCompression);

 // ������ ����������, �������� ������ ���� ��� ����������� �����. ���� �� ��� ��� ������������������� ����� ������ - ���� ����� ��� ��������
 if xDoCompress then
   begin
     // (3) calculate checksum and digest
     MD5Checksum(Source, SourceSize, @Checksum);

     if ACompressionType = rctFastest then
      xCompression := clLowest
     else
      xCompression := clHighest;

     // (4) compress input data
     Result:= CompressData(Source, SourceSize, Data, DataSize, xCompression);
     if (not Result) then Exit;

     // � ����� ������ ������ �� ������������
     {// (5) Apply security
     if (FPassSize > 0) then
      begin
       // -> generate random IV keys
       Subkeys[0]:= Round(Random * High(Longword));
       Subkeys[1]:= Round(Random * High(Longword));
       // -> encrypt compressed data
       BlowfishEncode(Data, DataSize, @PassBlock, FPassSize, Subkeys[0], Subkeys[1]);
      end else
      begin
       Subkeys[0]:= 0;
       Subkeys[1]:= 0;
      end;}

      Subkeys[0]:= 0;
      Subkeys[1]:= 0;
   end
 else
  begin
    Data := Source;
    DataSize := SourceSize;

    if ACompressionType <> rctNoCompression then
      begin
        // ���������������, ����� ��������� ��������
        if not DecompressData(Source, SourceSize, xDecompressedBuf, AUncompressedSize) then
          Exit;

        MD5Checksum(xDecompressedBuf, AUncompressedSize, @Checksum);
       {$IFDEF UseImageLoadBuffer}
        u_ImagesMemory.FreeMem_Cache_TwoPiece(xDecompressedBuf, nil);
       {$ELSE}
        FreeMem(xDecompressedBuf, AUncompressedSize);
       {$ENDIF}

        SourceSize := AUncompressedSize;
      end
    else
      MD5Checksum(Data, DataSize, @Checksum);
  end;

 // (6) OPEN THE ARCHIVE for reading & writing
 try
  Stream:= TFileStream.Create(FFileName, fmOpenReadWrite or fmShareExclusive);
 except
  Result:= False;
  Exit;
 end;

 // (7) update ASDb info, in case it has been changed
 Result:= ReadASDbInfo(Stream);
 if (not Result) then
  begin
   Stream.Free();
   Exit;
  end;

 // (8) if the record still exists, we cannot proceed
 if (GetRecordNum(Key) <> -1) then
  begin
   Stream.Free();
   Result:= False;
   Exit;
  end;

 // (9) write the ENTIRE RECORD
 try
  // seek the record table position and write the record there!
  RecordOffset:= ASDbHeader.TableOffset;
  Stream.Seek(RecordOffset, soBeginning);

  // RECORD TYPE
  stWriteWord(Stream, RecordType);
  // ORIGINAL SIZE
  stWriteLongword(Stream, SourceSize);
  // PHYSICAL SIZE
  stWriteLongword(Stream, DataSize);
  // DATE & TIME
  stWriteDouble(Stream, CurDate);

  // Checksum: MD5 message-digest
  Stream.WriteBuffer(Checksum, SizeOf(Checksum));

  // Security Information
  //stWriteWord(Stream, Word(FPassSize > 0));
  //Stream.WriteBuffer(Subkeys, SizeOf(Subkeys));

  xFlags := (ACompressionType and $7FFF); //< �� ��������� ACompressionType ����� ������ 15 ���. ��� 1 ��� �� force ����
  if AForceCompressionType then
    xFlags := xFlags or $8000;
  stWriteWord(Stream, xFlags);

  stWriteWord(Stream, AColorFormat);
  stWriteWord(Stream, APatternCount);
  stWriteWord(Stream, APatternWidth);
  stWriteWord(Stream, APatternHeight);

  // RECORD DATA
  Stream.WriteBuffer(Data^, DataSize);
 except
  Result:= False;
  if xDoCompress then
   {$IFDEF UseImageLoadBuffer}
    u_ImagesMemory.FreeMem_Cache_TwoPiece(Data, nil);
   {$ELSE}
    FreeMem(Data);
   {$ENDIF}
  Stream.Free();
  Exit;
 end;

 // (10) add new record to the record list
 NewIndex:= Length(FRecords);
 SetLength(FRecords, NewIndex + 1);

 if (DoForceExistRecordIndexOnReWrite) and (xIndex <> -1) then
  begin
    Move(FRecords[xIndex], FRecords[xIndex + 1], (Length(FRecords) - (xIndex + 1)) * SizeOf(TRecordInfo));
    NewIndex := xIndex;
  end;

 FRecords[NewIndex].Key:= Key;
 Move(Checksum, FRecords[NewIndex].Checksum, SizeOf(Checksum));
 FRecords[NewIndex].RecordType:= RecordType;
 FRecords[NewIndex].OrigSize:= SourceSize;
 FRecords[NewIndex].PhysSize:= DataSize;
 FRecords[NewIndex].Offset  := RecordOffset;
 FRecords[NewIndex].DateTime:= CurDate;

 FRecords[NewIndex].ForceCompressionType := AForceCompressionType;
 FRecords[NewIndex].CompressionType := ACompressionType;
 FRecords[NewIndex].ColorFormat := AColorFormat;
 FRecords[NewIndex].PatternCount := APatternCount;
 FRecords[NewIndex].PatternWidth := APatternWidth;
 FRecords[NewIndex].PatternHeight := APatternHeight;

 //FRecords[NewIndex].Secure  := (FPassSize > 0);
 //FRecords[NewIndex].InitBlock[0]:= Subkeys[0];
 //FRecords[NewIndex].InitBlock[1]:= Subkeys[1];

 // (11) update ASDb Header information
 ASDbHeader.TableOffset:= Stream.Position;
 ASDbHeader.RecordCount:= ASDbHeader.RecordCount + 1;

// SwitchRecords();
 try
  // (12) rewrite entire RECORD TABLE
  for i:= 0 to Length(FRecords) - 1 do
   begin
    stWriteString(Stream, FRecords[i].Key);
    stWriteLongword(Stream, FRecords[i].Offset);
   end;

 // (13) write down ASDb HEADER
 Stream.Seek(0, soFromBeginning);
 Stream.WriteBuffer(ASDbHeader, SizeOf(TASDbHeader));
 except
  Result:= False;
 end;

 // (14) Release the stream and memory

 if xDoCompress then
   {$IFDEF UseImageLoadBuffer}
    u_ImagesMemory.FreeMem_Cache_TwoPiece(Data, nil);
   {$ELSE}
    FreeMem(Data);
   {$ENDIF}

 Stream.Free();
end;

//---------------------------------------------------------------------------
function TASDb.WriteStream(const Key: string; Stream: TStream;
 RecordType: Integer; AForceDateTime : TDateTime = 0; ASourceIsCompressed : boolean = false; AUncompressedSize : cardinal = 0;
 ACompressionType : Word = rctMax; AForceCompressionType : boolean = false; AColorFormat : Word = 0; APatternCount : Word = 0; APatternWidth : Word = 0; APatternHeight : Word = 0): Boolean;
var
 Data: Pointer;
 DataSize, ReadBytes: Integer;
begin
 Result:= False;

 // verify open mode
 if (FOpenMode = opReadOnly) then Exit;

 // allocate memory for stream data
 DataSize:= Stream.Size - Stream.Position;
 Data:= AllocMem(DataSize);

 // read the stream data
 ReadBytes:= Stream.Read(Data^, DataSize);
 if (ReadBytes <> DataSize) then
  begin
   FreeMem(Data);
   Exit;
  end;
  
 // write the data to ASDb
 Result:= WriteRecord(Key, Data, DataSize, RecordType, AForceDateTime, ASourceIsCompressed, AUncompressedSize, ACompressionType, AForceCompressionType, AColorFormat, APatternCount, APatternWidth, APatternHeight);

 // free the unused memory
 FreeMem(Data);
end;

//---------------------------------------------------------------------------
function TASDb.WriteString(const Key, Text: string; RecordType: Integer): Boolean;
begin
 if (Length(Text) < 1) then
  begin
   Result:= False;
   Exit;
  end;

 Result:= WriteRecord(Key, @Text[1], Length(Text), RecordType);
end;

function TASDb.ReadCompressedRecord(const AKey: string; AData : pointer) : boolean;
var
  xStreamReadTime : cardinal;
begin
  Result := ReadCompressedRecord(xStreamReadTime, AKey, AData);
end;


function GetCurrentAppTime_Sec: Extended;
var
  xHighFreq: Int64;
  xCounter64: Int64;
begin
  QueryPerformanceFrequency(xHighFreq);
  QueryPerformanceCounter(xCounter64);
  Result := (xCounter64{ - UStartCounter64}) / xHighFreq;
end;

function TASDb.ReadCompressedRecord(out AStreamReadTime : cardinal; const AKey: string; AData : pointer) : boolean;
var
 PreSize: Cardinal;
 Index: Integer;
 Stream: TStream;
 xTime : extended;
begin
  Result:= False;

  AStreamReadTime := 0;

  // (1) OPEN archive
  try
    Stream:= TFileStream.Create(FFileName, fmOpenRead or fmShareDenyWrite);
  except
    Exit;
  end;

  try
  {$IFDEF UseASDBCachedInfo}
  if not FRecordsReadOnce then
    begin
      Result:= ReadASDbInfo(Stream);
      if (not Result) then
        Exit;
    end;
  {$ELSE}
  Result:= ReadASDbInfo(Stream);
  if (not Result) then
    Exit;
  {$ENDIF}

    // (3) find record index
    Index:= GetRecordNum(AKey);
    if (Index = -1) then
      begin
        Result:= False;
        Exit;
      end;

    // (4) create temporary buffers
    PreSize:= FRecords[Index].PhysSize;

    // (5) read the ENTIRE RECORD
    try
      // seek the record position in the file
      Stream.Seek(FRecords[Index].Offset + DataOffset, soBeginning);

      xTime := GetCurrentAppTime_Sec;
      // read record data
      Stream.ReadBuffer(AData^, PreSize);

      AStreamReadTime := Round((GetCurrentAppTime_Sec - xTime) * 1000);
    except
      Result:= False;
      Exit;
    end;
  finally
    // close the file stream
    Stream.Free();
  end;


  if FRecords[Index].CompressionType = rctOldSecure then
    begin
      // ������ ������� � secure �� ��������������
      Result := false;
      Exit;
    end;

  // (6) Apply security
  {if (FRecords[Index].Secure) and (FPassSize > 0) then
    begin
      BlowfishDecode(AData, PreSize, @PassBlock, FPassSize,
      FRecords[Index].InitBlock[0], FRecords[Index].InitBlock[1]);
    end;}

  Result:= true;
end;

function TASDb.ReadCompressedRecord(const AKey: string; out AData : pointer; out ADataSize : cardinal) : boolean;
var
  Index : integer;
begin
  Index:= GetRecordNum(AKey);
    if (Index = -1) then
      begin
        Result:= False;
        Exit;
      end;

  // assign data size
  ADataSize:= FRecords[Index].PhysSize;

  GetMem(AData, ADataSize);

  Result := ReadCompressedRecord(AKey, AData);

  if not Result then
    FreeMem(AData, ADataSize);
end;

function TASDb.ReadCompressedRecord(const AKey: string; out AData : pointer; out ADataSize : cardinal; out AStreamReadTime : cardinal) : boolean;
var
  Index : integer;
begin
  AStreamReadTime := 0;

  Index:= GetRecordNum(AKey);
    if (Index = -1) then
      begin
        Result:= False;
        Exit;
      end;

  // assign data size
  ADataSize:= FRecords[Index].PhysSize;

  GetMem(AData, ADataSize);

  Result := ReadCompressedRecord(AStreamReadTime, AKey, AData);

  if not Result then
    FreeMem(AData, ADataSize);
end;

{function TASDb._Debug_CachedIndex_ReadCompressedRecord(out AStreamReadTime : extended; const AKey: string; AData : pointer) : boolean;
var
 PreSize: Cardinal;
 Index: Integer;
 Stream: TStream;
begin
  Result:= False;

  AStreamReadTime := 0;

  // (1) OPEN archive
  try
    Stream:= TFileStream.Create(FFileName, fmOpenRead or fmShareDenyWrite);
  except
    Exit;
  end;

  try
    // (2) update ASDb info, in case it has been changed
    //Result:= ReadASDbInfo(Stream);
    //if (not Result) then
      //Exit;

    // (3) find record index
    Index:= GetRecordNum(AKey);
    if (Index = -1) then
      begin
        Result:= False;
        Exit;
      end;

    // (4) create temporary buffers
    PreSize:= FRecords[Index].PhysSize;

    // (5) read the ENTIRE RECORD
    try
      // seek the record position in the file
      Stream.Seek(FRecords[Index].Offset + DataOffset, soFromBeginning);

      AStreamReadTime := GetCurrentAppTime_Sec;
      // read record data
      Stream.ReadBuffer(AData^, PreSize);

      AStreamReadTime := (GetCurrentAppTime_Sec - AStreamReadTime) * 1000;
    except
      Result:= False;
      Exit;
    end;
  finally
    // close the file stream
    Stream.Free();
  end;


  if FRecords[Index].CompressionType = rctOldSecure then
    begin
      // ������ ������� � secure �� ��������������
      Result := false;
      Exit;
    end;

  Result:= true;
end;

function TASDb._Debug_CachedIndex_ReadCompressedRecord(const AKey: string; out AData : pointer; out ADataSize : cardinal; out AStreamReadTime : extended) : boolean;
var
  Index : integer;
begin
  AStreamReadTime := 0;

  Index:= GetRecordNum(AKey);
    if (Index = -1) then
      begin
        Result:= False;
        Exit;
      end;

  // assign data size
  ADataSize:= FRecords[Index].PhysSize;

  GetMem(AData, ADataSize);

  Result := _Debug_CachedIndex_ReadCompressedRecord(AStreamReadTime, AKey, AData);

  if not Result then
    FreeMem(AData, ADataSize);
end;}

class function TASDb.DecompressRecord(ACompressedData : pointer; ACompressedDataSize: cardinal; ADecompressedDataSize: cardinal; out ADecompressedData: pointer) : boolean;
begin
  // (7) decompress the data stream
  Result:= DecompressData(ACompressedData, ACompressedDataSize, ADecompressedData, ADecompressedDataSize);
end;

class function TASDb.CompressRecord(ADecompressedData : pointer; ADecompressedDataSize: cardinal; out ACompressedData: pointer; out ACompressedDataSize: cardinal; ACompressionLevel : TCompressionLevel = clHighest) : boolean;
begin
  Result:= CompressData(ADecompressedData, ADecompressedDataSize, ACompressedData, ACompressedDataSize, ACompressionLevel);
end;

//---------------------------------------------------------------------------
function TASDb.ReadRecord(const Key: string; out Data: Pointer;
 out DataSize: Cardinal): Boolean;
var
 PreBuf: Pointer;
 PreSize: Cardinal;
 Index: Integer;
 Stream: TStream;
 Checksum: array[0..3] of Longword;

 // xTime : cardinal;

 xDoDecompress : boolean;
begin
  Result:= False;

  // (1) OPEN archive
  try
    Stream:= TFileStream.Create(FFileName, fmOpenRead or fmShareDenyWrite);
  except
    Exit;
  end;
  try
  //  xTime := GetTickCount;

    {$IFDEF UseASDBCachedInfo}
    if not FRecordsReadOnce then
      begin
        Result:= ReadASDbInfo(Stream);
        if (not Result) then
          Exit;
      end;
    {$ELSE}
    Result:= ReadASDbInfo(Stream);
    if (not Result) then
      Exit;
    {$ENDIF}

  //  UASDB_StatsisticsCriticalSection.Enter;
  //  UASDB_CurrentAccumulatedTimeForHeadersReading  := UASDB_CurrentAccumulatedTimeForHeadersReading  + (GetTickCount - xTime);
  //  UASDB_StatsisticsCriticalSection.Leave;

    // (3) find record index
    Index:= GetRecordNum(Key);
    if (Index = -1) then
      begin
        Result:= False;
        Exit;
      end;

    // assign data size
    DataSize:= FRecords[Index].OrigSize;

    if DataSize = 0 then
      begin
        // � ������, ���� �������� ������ ������ = 0, �������, ��� ������ � ���������� ������ ������ (�������� ������ ��� ������)
        Data := nil;
        DataSize := 0;
        Result := true;
        Exit;
      end;

    xDoDecompress := FRecords[Index].CompressionType <> rctNoCompression;

    // 2015.08.19 - Corris: bugfix - ��������� PreSize ��� �������� ������ � �������� Max � ������ ����
    // (4) create temporary buffers
    PreSize:= FRecords[Index].PhysSize;
   {$IFDEF UseImageLoadBuffer}
    // ���������� ����� ��������
    u_ImagesMemory.RequestSession_Cache_TwoPiece(Max(DataSize, PreSize),  not xDoDecompress, FileName + '  :  ' + Key);
   {$ENDIF}

   {$IFDEF UseImageLoadBuffer}
     PreBuf := u_ImagesMemory.GetMem_Cache_TwoPiece(PreSize, Self, 'PreBuf');
   {$ELSE}
     GetMem(PreBuf, PreSize);
   {$ENDIF}

    // (5) read the ENTIRE RECORD
    try
  //    xTime := GetTickCount;

      // seek the record position in the file
      Stream.Seek(FRecords[Index].Offset + DataOffset, soBeginning);

      // read record data
      Stream.ReadBuffer(PreBuf^, PreSize);

  //    UASDB_StatsisticsCriticalSection.Enter;
  //    UASDB_CurrentAccumulatedTimeForStreamReading  := UASDB_CurrentAccumulatedTimeForStreamReading  + (GetTickCount - xTime);
  //    UASDB_CurrentStreamsReadedAccumulatedSize := UASDB_CurrentStreamsReadedAccumulatedSize + PreSize;
  //    Inc(UASDB_CurrentStreamsReadedCount);

      //DebugFileAdd('C:\WinCVSWork\Game\Logs\readrecord_stats.csv', FileName + '_' + Key + ';' + IntToStr(PreSize));
  //    UASDB_StatsisticsCriticalSection.Leave;
    except
      Result:= False;

     {$IFDEF UseImageLoadBuffer}
       u_ImagesMemory.ReleaseSession_Cache_TwoPiece;

       u_ImagesMemory.FreeMem_Cache_TwoPiece(PreBuf, Self);
     {$ELSE}
       FreeMem(PreBuf);
     {$ENDIF}
      Exit;
    end;
  finally
    // close the file stream
    Stream.Free();
  end;

  try

    if FRecords[Index].CompressionType = rctOldSecure then
    begin
      // ������ ������� � secure �� ��������������
      Result := false;
      Exit;
    end;

    {// (6) Apply security
    if (FRecords[Index].Secure)and(FPassSize > 0) then
      begin
        BlowfishDecode(PreBuf, PreSize, @PassBlock, FPassSize,
        FRecords[Index].InitBlock[0], FRecords[Index].InitBlock[1]);
      end;}

  //  xTime := GetTickCount;

    // (7) decompress the data stream
    if xDoDecompress then
      Result:= DecompressData(PreBuf, PreSize, Data, DataSize)
    else
      begin
        Data := PreBuf;
        DataSize := PreSize;
        Result := true;
      end;
      
  //  UASDB_StatsisticsCriticalSection.Enter;
  //  UASDB_CurrentAccumulatedTimeForDecompressing := UASDB_CurrentAccumulatedTimeForDecompressing + (GetTickCount - xTime);
  //  UASDB_StatsisticsCriticalSection.Leave;

    if (not Result) then
      Exit;

  finally
   {$IFDEF UseImageLoadBuffer}
      u_ImagesMemory.ReleaseSession_Cache_TwoPiece;
   {$ENDIF}
    if xDoDecompress then
      begin
        // (8) release buffers
        {$IFDEF UseImageLoadBuffer}
          u_ImagesMemory.FreeMem_Cache_TwoPiece(PreBuf, Self);
        {$ELSE}
          FreeMem(PreBuf);
        {$ENDIF}
      end;
  end;


  if FDoCompareCheckSumm then
    begin
      // (9) checksum verification
      MD5Checksum(Data, DataSize, @Checksum);
      Result:= CompareMem(@Checksum, @FRecords[Index].Checksum, SizeOf(Checksum));
    end
  else
    Result := true;
end;


{$IFDEF USE_MEMORYHOLDER}
function TASDb.ReadRecordUsingInterprocessedDataCatch(const Key: string; out Data: Pointer;
  out DataSize: Cardinal): Boolean;
var
  xCompressedData : Pointer;
  xCompressedDataSize : integer;
  xUncompressedDatasize : integer;
begin
  if UInterprocessDataCache.GetCache_ASDBStream(FileName, Key, xCompressedData, xCompressedDataSize, xUncompressedDatasize) then
    begin
      // ����� ������� ��� ��� ����� ������ �������� � ��������� ������ �����������

      DataSize := xUncompressedDatasize;

      if xCompressedDataSize <> xUncompressedDatasize then
        begin
          Result := DecompressRecord(xCompressedData, xCompressedDataSize, xUncompressedDatasize, Data);

          try
            // 2015.05.19 - Corris: ���������������, ��� Result ������ True, ���� ������ ������ �����. ���� ��� - �� false (����� ����).
            // � ������ ������ � MemHolder - ������������ ����� ���� ������ � ��� ������. �� ������ ������ ���������.
            Assert(Result, 'TASDb.ReadRecordUsingInterprocessedDataCatch: DecompressRecord(xCompressedData, xCompressedDataSize, xUncompressedDatasize, Data);  return FALSE')
          except
            try
              {Result := }DecompressRecord(xCompressedData, xCompressedDataSize, xUncompressedDatasize, Data);
            except
            end;
            // ����������� ������ - ��� ��������, ��������� �� ��������� ������� (���� ��- ������ ������ � �������)
            Result := ReadRecord(Key, Data, DataSize);
          end;

         {$IFDEF UseImageLoadBuffer}
          u_ImagesMemory.FreeMem_Cache_TwoPiece(xCompressedData, nil);
         {$ELSE}
          FreeMem(xCompressedData, xCompressedDataSize);
         {$ENDIF}
        end
      else
        begin
          Data := xCompressedData;
          Result := true;
        end;
    end
  else
    Result := ReadRecord(Key, Data, DataSize);
end;
{$ENDIF}


//---------------------------------------------------------------------------
function TASDb.ReadStream(const Key: string; Stream: TStream): Boolean;
var
 Data: Pointer;
 DataSize: Cardinal;
 {$IFnDEF UseImageLoadBuffer}
 BytesWritten: Cardinal;
 {$ENDIF}
begin
 // read the record data
 {$IFDEF USE_MEMORYHOLDER}
 Result:= ReadRecordUsingInterprocessedDataCatch(Key, Data, DataSize);
 {$ELSE}
 Result := ReadRecord(Key, Data, DataSize);
 {$ENDIF}
 

 // write the record data to stream
 if (Result) then
  begin
   // free the unused memory

   {$IFDEF UseImageLoadBuffer}

    // ��� ������ ����� ��� ��� �����,  � �� ����, ����� ������ ��������� ��� ����� ��� ������ (�� �������)
    if (Stream is TMemoryBuffer_Stream) and
      (TMemoryBuffer_Stream(Stream).Size = 0) then
        TMemoryBuffer_Stream(Stream).AcquireMemory(Data, DataSize)
    else
      Stream.Write(Data^, DataSize);

    u_ImagesMemory.FreeMem_Cache_TwoPiece(Data, nil);

   {$ELSE}

    BytesWritten:= Stream.Write(Data^, DataSize);
    Result:= (BytesWritten = DataSize);
    FreeMem(Data);

   {$ENDIF}

  end;
end;

//---------------------------------------------------------------------------
function TASDb.ReadString(const Key: string; out Text: string): Boolean;
var
 Data: Pointer;
 Size: Cardinal;
begin
 Result:= ReadRecord(Key, Data, Size);
 if (Result) then
  begin
   if (Size > 0) then
    begin
     SetLength(Text, Size);
     Move(Data^, (@Text[1])^, Size);
     {$IFDEF UseImageLoadBuffer}
      u_ImagesMemory.FreeMem_Cache_TwoPiece(Data, nil);
     {$ELSE}
      FreeMem(Data);
     {$ENDIF}
    end else Text:= '';
  end;
end;

//---------------------------------------------------------------------------
function TASDb.RemoveRecord(const Key: string): Boolean;
var
 InStream, OutStream: TFileStream;
 NewHeader: TASDbHeader;
 NewRecords: array of TRecordInfo;
 i, Index, NewIndex: Integer;
 Data: Pointer;
 DataSize: Cardinal;
 xTempFileName : string;
begin
 SetLength(NewRecords, 0);
 Data:= nil;

 // (1) Update record list
 Result:= Update();
 if (not Result) then Exit;

 // (2) retreive record index
 Index:= GetRecordNum(Key);
 if (Index = -1) then
  begin
   Result:= False;
   Exit;
  end; 

 // (3) OPEN THE SOURCE for reading & writing
 try
  InStream:= TFileStream.Create(FFileName, fmOpenReadWrite or fmShareDenyWrite);
 except
  Exit;
 end;

 // (4) OPEN THE DESTINATION for writing
 try
  // 2016.02.17 - BeaR: ��� ��� ���� - �������� ���� ������������ � �������� ���������, � � ������ ���� ��� ����������� c:/windows/system32, ���������� �������,
  // ��� ��� ��� ���� �� ������ ����. ������ �������� ���� �������������� ��������� � ��� �� ����� ��� � ������� ����. ����� ���� ������� ������ ��������
  // ���� � ���� ���������� �����, �� ����� ���� ��� ���� �������������� ���� ����� �� ������� ���� �����, � ���� ���������� ������� �������� ������ � �������� �
  // ���������� �����. �� ���� ������ ��� �������� � ��� ����� ��� ������� ���������� � ���� ����������� � ������ �������� ������ � ���� ������ ��� ����������� -
  // � ����� ����� ���, ���� � ������ ������� �������� ���� ���� � ��� �� ����� ��� �������� ���� ����� - ���� ������ ����, �� ��������� �� ����� �������� ��� � ���� � ���
  xTempFileName := ExtractFileDir(FFileName) + '\temp.sdfsdf6sf5sf68sf678.tmp'; //< ��� ����������, ����� �� ���� ���������� (��� ������� ����� ���� ���� �������� ��� ������ ��������� ��� �������)

  OutStream:= TFileStream.Create(xTempFileName, fmCreate);
 except
  Exit;
 end;

 // (5) update ASDb info, in case it has been changed
 Result:= ReadASDbInfo(InStream);
 if (not Result) then
  begin
   InStream.Free();
   OutStream.Free();
   Exit;
  end;

 // (6) create NEW HEADER
 Move(ASDbHeader, NewHeader, SizeOf(TASDbHeader));
 NewHeader.RecordCount:= ASDbHeader.RecordCount - 1;

 // (7) Write temporary ASDb header
 try
  OutStream.WriteBuffer(NewHeader, SizeOf(TASDbHeader));
 except
  Result:= False;
  InStream.Free();
  OutStream.Free();
  Exit;
 end;

 // (8) Completely rewrite RECORD LIST
 for i:= 0 to Length(FRecords) - 1 do
  if (i <> Index) then
   begin
    // create a copy of previous record
    NewIndex:= Length(NewRecords);
    SetLength(NewRecords, NewIndex + 1);
    NewRecords[NewIndex]:= FRecords[i];

    // update record offset
    NewRecords[NewIndex].Offset:= OutStream.Position;

    // allocate temporary buffers
    DataSize:= NewRecords[NewIndex].PhysSize + DataOffset;
    ReallocMem(Data, DataSize);

    // read the whole record block
    try
     InStream.Seek(FRecords[i].Offset, soBeginning);
     InStream.ReadBuffer(Data^, DataSize);
    except
     InStream.Free();
     OutStream.Free();
     FreeMem(Data);
     Result:= False;
     Exit;
    end;

    // write the whole record block
    try
     OutStream.WriteBuffer(Data^, DataSize);
    except
     InStream.Free();
     OutStream.Free();
     FreeMem(Data);
     Result:= False;
     Exit;
    end;
   end; // rewrite records

 // the record table follows, update ASDb header
 NewHeader.TableOffset:= OutStream.Position;

 // (9) write NEW RECORD table (and update the current one)
 SetLength(FRecords, Length(NewRecords));
 try
  for i:= 0 to Length(NewRecords) - 1 do
   begin
    // write record info
    stWriteString(OutStream, NewRecords[i].Key);
    stWriteLongword(OutStream, NewRecords[i].Offset);

    // update the record table
    FRecords[i]:= NewRecords[i];
   end;

  // (10) write updated ASDb header
  OutStream.Seek(0, soFromBeginning);
  OutStream.WriteBuffer(NewHeader, SizeOf(TASDbHeader));
 except
  Result:= False;
  InStream.Free();
  OutStream.Free();
  Exit;
 end;

 // update file size
 FFileSize:= OutStream.Size;

 // (11) Release allocated buffers
 if (Data <> nil) then FreeMem(Data);
 InStream.Free();
 OutStream.Free();

 // (12) Switch between temporary file and real one
 try
  DeleteFile(FFileName);
  RenameFile(xTempFileName, FFileName);
 except
  Result:= False;
 end;
end;

//---------------------------------------------------------------------------
function TASDb.RenameRecord(const Key, NewKey: string): Boolean;
var
 Index: Integer;
begin
 // (1) Check the validity of OpenMode.
 if (FOpenMode in [opOverwrite, opReadonly]) then
  begin
   Result:= False;
   Exit;
  end;

 // (2) Refresh record list.
 Result:= ReadASDbInfo(nil);
 if (not Result) then Exit;

 // (3) Check the validity of specified keys.
 Index:= GetRecordNum(Key);
 if (Index = -1)or(GetRecordNum(NewKey) <> -1) then
  begin
   Result:= False;
   Exit;
  end;

 // (4) Modify record table.
 FRecords[Index].Key:= NewKey;

 // (5) Write new record table.
 Result:= WriteRecordTable();
end;

//---------------------------------------------------------------------------
function TASDb.SwitchRecords(Index1, Index2: Integer): Boolean;
var
 Aux: TRecordInfo;
begin
 // (1) Check the validity of OpenMode.
 if (FOpenMode in [opOverwrite, opReadonly]) then
  begin
   Result:= False;
   Exit;
  end;

 // (2) Refresh record list.
 Result:= ReadASDbInfo(nil);
 if (not Result) then Exit;

 // (3) Validate indexes with updated list.
 if (Index1 < 0)or(Index2 < 0)or(Index1 >= Length(FRecords))or
  (Index2 >= Length(FRecords)) then
  begin
   Result:= False;
   Exit;
  end;

 // (4) Exchange two records.
 Aux:= FRecords[Index1];
 FRecords[Index1]:= FRecords[Index2];
 FRecords[Index2]:= Aux;

 // (5) Write new record table.
 Result:= WriteRecordTable();
end;

//---------------------------------------------------------------------------
function TASDb.SortRecords(): Boolean;
var
 i, j: Integer;
 Aux: TRecordInfo;
begin
 for i:= 0 to Length(FRecords) - 2 do
  for j:= 0 to Length(FRecords) - i - 2 do
   if (FRecords[j].RecordType > FRecords[j + 1].RecordType) then
    begin
     Aux:= FRecords[j];
     FRecords[j]:= FRecords[j + 1];
     FRecords[j + 1]:= Aux;
    end;

 Result:= WriteRecordTable();
end;

function TASDb.ChangeCompression(const AKey : string; ACompressionType : word; AForceCompressionType : boolean) : boolean;
var
  xIndex : Integer;
  xFlags : word;
  xStream : TFileStream;

  xBuff : pointer;
  xBuffSize : cardinal;

  xImageHeader : TAsphyreImageHeader;
begin
  try
    xStream:= TFileStream.Create(FFileName, fmOpenReadWrite or fmShareExclusive);
  except
    Result:= False;
    Exit;
  end;

  Result:= ReadASDbInfo(xStream);
  if (not Result) then
    begin
      xStream.Free;
      Exit;
    end;

  xIndex := GetRecordNum(AKey);

  if (xIndex = -1) then
    begin
      xStream.Free;
      Result:= False;
      Exit;
    end;

  if (FRecords[xIndex].CompressionType = ACompressionType) and (FRecords[xIndex].ForceCompressionType = AForceCompressionType) then
    begin
      xStream.Free;
      Result := true;
      Exit;
    end;

  if (FRecords[xIndex].CompressionType = ACompressionType) then
    begin
      // ���������� ������ ForceCompressionType ����, ����� �� �������������� ���� �����

      FRecords[xIndex].ForceCompressionType := AForceCompressionType;

      xStream.Seek(
        FRecords[xIndex].Offset + //< �������� �� ������ ��������� �������
        SizeOf(Word) + //< record type
        SizeOf(Longword) + //< original size
        SizeOf(Longword) + //< physical size
        Sizeof(Double) + //< Data & time
        Sizeof(Longword) * 4, //< checksum (longword[4])
        soBeginning);

      xFlags := (ACompressionType and $7FFF); //< �� ��������� ACompressionType ����� ������ 15 ���. ��� 1 ��� �� force ����
      if AForceCompressionType then
        xFlags := xFlags or $8000;
      stWriteWord(xStream, xFlags);

      xStream.Free;
      Result := true;
      Exit;
    end
  else
    begin
      // � ���� ������ ����� ������������������� �����, ������� ��������� ����� � ������������� � ���������� ��� � ������ �����������

      xStream.Free;

      // ������ �� ��������� ������ ����� � ������������������� ����
      if not ReadRecord(AKey, xBuff, xBuffSize) then
        begin
          Result := false;
          Exit;
        end;

      if (FRecords[xIndex].CompressionType = rctOldNoSecure) or (FRecords[xIndex].CompressionType = rctOldSecure) then
        begin
          // ���� ��������� �� ������� ������� � �����, �� ���� ������� ������ ��� ������ � ����� ��������� � ���������

          if (FRecords[xIndex].RecordType = recGraphics) then
            begin
              if not ReadImageHeader(AKey, xImageHeader) then
                begin
                 {$IFDEF UseImageLoadBuffer}
                  u_ImagesMemory.FreeMem_Cache_TwoPiece(xBuff, nil);
                 {$ELSE}
                  FreeMem(xBuff, xBuffSize);
                 {$ENDIF}
                  Result := false;
                  Exit;
                end;

              Result := WriteRecord(AKey, xBuff, xBuffSize, recGraphics, 0, false, 0, ACompressionType, AForceCompressionType,
                Word(xImageHeader.InFormat), xImageHeader.PatternCount, xImageHeader.PatternSize.X, xImageHeader.PatternSize.Y);
            end
          else
            begin
              Result := WriteRecord(AKey, xBuff, xBuffSize, FRecords[xIndex].RecordType, 0, false, 0, ACompressionType, AForceCompressionType,
                0, 0, 0, 0);
            end;
        end
      else
        begin
          // ���������� ������ ��� ������ � ������ ����� ������ � force ������, ��������� ������ ������� �� �������� ��������� ������
          Result := WriteRecord(AKey, xBuff, xBuffSize, FRecords[xIndex].RecordType, 0, false, 0, ACompressionType, AForceCompressionType,
            FRecords[xIndex].ColorFormat, FRecords[xIndex].PatternCount, FRecords[xIndex].PatternWidth, FRecords[xIndex].PatternHeight);
        end;

     {$IFDEF UseImageLoadBuffer}
      u_ImagesMemory.FreeMem_Cache_TwoPiece(xBuff, nil);
     {$ELSE}
      FreeMem(xBuff, xBuffSize);
     {$ENDIF}
      Exit;
    end;
end;

//---------------------------------------------------------------------------
{ TASDbCachedInfo_Item }

constructor TASDbCachedInfo_Item.Create;
begin
end;

initialization
  {$IFDEF UseASDBCachedInfo}
  // ������������ ��������� ASDB ������, ��� ��������� ������ � ����
  UASDbCachedInfo := TASDbCachedInfo.Create;
  {$ENDIF}

//  UASDB_StatsisticsCriticalSection := TCriticalSection.Create;
finalization
//  Destnil(UASDB_StatsisticsCriticalSection);

  {$IFDEF UseASDBCachedInfo}
  Destnil(UASDbCachedInfo);
  {$ENDIF}
end.
