unit DispatcherUnit;

interface
  uses
    Classes,
    GameUtils,
    AsphyreCanvas,
    ExtCtrls;

type
  TGameDispatcher = class
    public
      constructor Create; 
      destructor Destroy; override;
      procedure AddGenerator(generator: TGameObjectGenerator;
        CoordGenerator: TCoordGenerator; PosConstraint: TCoordConstraint);
      procedure IncrementMovements; virtual;
      procedure DrawObjects(canvas: TAsphyreCanvas); virtual;
      procedure SetSpeedNorm(NewSpeed: integer);
    private
      Generators: TList;
  end;

implementation


{ TGameDispatcher }

procedure TGameDispatcher.AddGenerator(generator: TGameObjectGenerator;
  CoordGenerator: TCoordGenerator; PosConstraint: TCoordConstraint);
begin
  generator.SetCoordGenerator(CoordGenerator);
  generator.SetPosConstraint(PosConstraint);
  Generators.Add(generator);
end;

constructor TGameDispatcher.Create;
begin
  Generators := TList.Create;
end;

destructor TGameDispatcher.Destroy;
var
  i: integer;
begin
  inherited Destroy; 
  for i:=0 to Generators.Count - 1 do
     TGameObjectGenerator(Generators[i]).Free;
  Generators.Clear;
  Generators.Destroy;
end;

procedure TGameDispatcher.DrawObjects(canvas: TAsphyreCanvas);
var
  i: integer;
  obj: TGameObjectGenerator;
begin
  for i:=0 to Generators.Count -1 do
    begin
      obj := TGameObjectGenerator(Generators[i]) as TGameObjectGenerator;
      obj.DrawAll(canvas);
    end;
end;

procedure TGameDispatcher.IncrementMovements;
var
  i: integer;
begin
  for i:=0 to Generators.Count -1 do
    TGameObjectGenerator(Generators[i]).IncrementMovements;
end;

procedure TGameDispatcher.SetSpeedNorm(newSpeed: integer);
var
  i: integer;
begin
  for i:=0 to Generators.Count -1 do
    TGameObjectGenerator(Generators[i]).SetSpeedNorm(newSpeed);
end;

end.
