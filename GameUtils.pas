unit GameUtils;

interface
  uses
    AsphyreCanvas,
    AsphyreDef,
    AsphyreImages,
    AsphyreTimers,
    ExtCtrls,
    Math,
    Classes;

type
  // ��� ���������
  RPoint = Record
    x : real;
    y : real;
  end;

  TCoordConstraint = function(pos: RPoint): boolean of object;
  PCoordConstraint =  ^TCoordConstraint;
  TCoordGenerator = function(): RPoint of object;

// ############################################################################

    // ������� ����� ��� �������� �� ������
  TBaseObject = class
    public
      Coord: RPoint;

      constructor Create(objName: string; objImage : TAsphyreImage);
      procedure Draw(canvas: TAsphyreCanvas); virtual; //< ������ ������ �� ������
      procedure Move; virtual; abstract; //< ������� ������
    private
      Name : string; //< ��� �������
      Image : TAsphyreImage; //< ������-����������� �������
  end;
  PBaseObject = ^TBaseObject;
  TFeeder = function(): TBaseObject of object;

    // �����
  TGun = class(TBaseObject)
    public
      procedure Move; overload; override;
      procedure Move(delta: RPoint); reintroduce; overload;
      procedure SetPosConstraint(checker: TCoordConstraint);
    private
      CoordConstraint: TCoordConstraint;
  end;

  // ����������
  TParatrooper = class(TBaseObject)
    public
      constructor Create(objImage : TAsphyreImage; SpeedNorm: integer);
      procedure Move; overload; override;
    private
      FallSpeed: real;
  end;
  PParatrooper = ^TParatrooper;

  TOnShot = procedure(Sender: TBaseObject; SenderTarget: TBaseObject) of object;
  TBullet = class(TBaseObject)
    public
      OnShot: TOnShot;
      OnMiss: TOnShot;
      
      constructor Create(aim: TBaseObject; objImage : TAsphyreImage;
               SpeedNorm: integer);
      procedure Move; overload; override;
      function HitTest() : boolean; virtual;
      procedure SetAccuracy(newAccuracy: real);
      procedure SetFlyingAngle;
    private
      Speed: real;
      Target: TBaseObject;
      TargetHit: boolean;
      Accuracy: real;
      FlyAngle : real;
  end;

// ############################################################################

  TGameObjectGenerator = class
    public
      constructor Create(objImage : TAsphyreImage); virtual;
      destructor Destroy; override;
      procedure Spawn; virtual; abstract;
      procedure RemoveObject(obj: TBaseObject); virtual;
      procedure DrawAll(canvas: TAsphyreCanvas); virtual;
      procedure SetCoordGenerator(coordGen: TCoordGenerator);
      procedure EnableSpawn(enabled: boolean);
      procedure IncrementMovements; virtual;
      procedure SetPosConstraint(checker: TCoordConstraint);
      function Count(): integer;
      procedure SetSpeedNorm(speed: integer);
      procedure SetSpawnInterval(SpawnFrequency: integer);
    private
      GameObjectList: TList;
      RemoveList: TList;
      CoordGenerator: TCoordGenerator;
      CoordConstraint: TCoordConstraint;
      Image: TAsphyreImage;
      SpawnTimer: TTimer;
      SpeedNorm: integer;

      procedure OnSpawn(Sender : TObject);
  end;
  PGameObjectGenerator = ^TGameObjectGenerator;

  TParatrooperGenerator = class(TGameObjectGenerator)
    public
      constructor Create(objImage : TAsphyreImage); override;
      procedure Spawn; override;
      function NextAim: TBaseObject;
      procedure RemoveObject(obj: TBaseObject); override;
      procedure RenewAsTarget(obj: TBaseObject);
    private
      AimIndex: integer;
  end;

  TBulletGenerator = class(TGameObjectGenerator)
    public
      constructor Create(objImage : TAsphyreImage); override;
      procedure Spawn; override;
      procedure SetFeeder(aimFeeder: TFeeder);
      procedure SetHitCallback(callback: TOnShot);
      procedure SetMissCallback(callback: TOnShot);
    private
      OnShot: TOnShot;
      OnMiss: TOnShot;
      Feeder: TFeeder;
  end;

implementation

// --------------------- { TBaseObject  }  ----------------------------------//

// ������ ������ �� ������
constructor TBaseObject.Create(objName: string; objImage: TAsphyreImage);
begin
  Name := objName;
  Image := objImage;
end;

procedure TBaseObject.Draw(canvas: TAsphyreCanvas);
begin
  // ������� ������ �� �����
  if Image <> nil then
    canvas.DrawRot(Image, Coord.x, Coord.y, 0, 1, 0, fxBlend);
end;


// ---------------------------------------------------------------------------//


// ---------------------  { TGun }  ------------------------------------------//

procedure TGun.SetPosConstraint(checker: TCoordConstraint);
begin
  coordConstraint := checker;
end;

procedure TGun.Move;
begin
end;

procedure TGun.Move(delta: RPoint);
var
  newCoord: RPoint;
begin
  newCoord.x := Coord.x + delta.x;
  newCoord.y := Coord.y + delta.y;

  {$WARN UNSAFE_CODE OFF}
  if (@coordConstraint <> nil) then
    begin
      if CoordConstraint(newCoord) then
        Coord := newCoord;
    end
  else
     Coord := newCoord;
  {$WARN UNSAFE_CODE ON}
end;

// ---------------------------------------------------------------------------//

// --------------------- { TParatrooper } ------------------------------------//

constructor TParatrooper.Create(objImage: TAsphyreImage; SpeedNorm: integer);
begin
  inherited Create('smth', objImage);
  FallSpeed := (15.0 + Random * 40.0) / SpeedNorm;
end;

procedure TParatrooper.Move;
begin
  Coord.y := Coord.y + FallSpeed;
end;

// ---------------------------------------------------------------------------//

// --------------------- { TGameObjectGenerator }-----------------------------//

constructor TGameObjectGenerator.Create(objImage : TAsphyreImage);
begin
  GameObjectList := TList.Create;
  RemoveList := TList.Create;
  Image := objImage;

  SpeedNorm := 1000;
  SpawnTimer := TTimer.Create(nil);
  SpawnTimer.Interval := 600;
  SpawnTimer.OnTimer := Self.OnSpawn;
end;

destructor TGameObjectGenerator.Destroy;
var
  i : integer;
begin
 inherited Destroy;
 SpawnTimer.Destroy;

 for i:= 0 to GameObjectList.Count - 1 do
  TBaseObject(GameObjectList[i]).Free;
 for i:= 0 to  RemoveList.Count - 1 do
  TBaseObject(RemoveList[i]).Free;
end;

procedure TGameObjectGenerator.DrawAll(canvas: TAsphyreCanvas);
var
 i: integer;
begin
  for i := 0 to GameObjectList.Count-1 do
    (TBaseObject(GameObjectList.Items[i]) as TBaseObject).Draw(canvas);
end;

procedure TGameObjectGenerator.EnableSpawn(enabled: boolean);
begin
  SpawnTimer.Enabled := enabled;
end;

procedure TGameObjectGenerator.OnSpawn(Sender: TObject);
begin
 Spawn;
end;

procedure TGameObjectGenerator.SetCoordGenerator(coordGen: TCoordGenerator);
begin
 CoordGenerator := coordGen;
end;


function TGameObjectGenerator.Count: integer;
begin
  Result := GameObjectList.Count;
end;

procedure TGameObjectGenerator.SetSpeedNorm(speed: integer);
begin
  SpeedNorm := speed;
end;

procedure TGameObjectGenerator.SetSpawnInterval(SpawnFrequency: integer);
begin
  SpawnTimer.Interval := SpawnFrequency;
end;

procedure TGameObjectGenerator.IncrementMovements;
var
  i: integer;
  rm: TBaseObject;
begin
  while RemoveList.Count > 0 do
    begin
      rm := RemoveList[0];
      if GameObjectList.IndexOf(rm) <> -1 then
        begin
          rm.Free;
          GameObjectList.Remove(rm);
        end;
      RemoveList.Delete(0);
    end;

  for i := 0 to GameObjectList.Count-1 do
    (TBaseObject(GameObjectList.Items[i]) as TBaseObject).Move;

  {$WARN UNSAFE_CODE OFF}
  if (@CoordConstraint <> nil) then
    begin
      i:=0;
      while i < GameObjectList.Count do
        begin
          if not CoordConstraint(TBaseObject(GameObjectList.Items[i]).Coord) then
            begin
              rm := GameObjectList.Items[i];
              rm.Free;
              GameObjectList.Remove(rm);
            end
          else
           Inc(i);
        end;
    end;
  {$WARN UNSAFE_CODE ON}
end;

procedure TGameObjectGenerator.SetPosConstraint(checker: TCoordConstraint);
begin
  CoordConstraint := checker;
end;

procedure TGameObjectGenerator.RemoveObject(obj: TBaseObject);
begin
  RemoveList.Add(obj);
end;

// ---------------------------------------------------------------------------//

// --------------------- { TParatrooperGenerator }----------------------------//


constructor TParatrooperGenerator.Create(objImage: TAsphyreImage);
begin
  inherited Create(objImage);
  AimIndex := -1;
end;

function TParatrooperGenerator.NextAim: TBaseObject;
begin
  if AimIndex = -1 then
    Result := nil
  else
    begin
      Result := GameObjectList[AimIndex];
      Inc(AimIndex);
      if AimIndex >= GameObjectList.Count then
        AimIndex := -1;
    end;
end;

procedure TParatrooperGenerator.RemoveObject(obj: TBaseObject);
begin
  RemoveList.Add(obj);
  if AimIndex >= 0 then
    Dec(AimIndex);
end;

procedure TParatrooperGenerator.RenewAsTarget(obj: TBaseObject);
var
 tmp : TBaseObject;
begin
  if AimIndex <> -1 then
    begin
      tmp := TBaseObject(GameObjectList.Extract(obj));
      GameObjectList.Add(tmp);
      AimIndex := GameObjectList.Count - 1;
    end;
end;

procedure TParatrooperGenerator.Spawn;
var
  paratrooper: TParatrooper;
begin
  paratrooper := TParatrooper.Create(Image, SpeedNorm);
  {$WARN UNSAFE_CODE OFF}
  if (@CoordGenerator <> nil) then
    begin
      paratrooper.Coord := CoordGenerator();
    end;
  {$WARN UNSAFE_CODE ON}
  GameObjectList.Add(paratrooper);
  if AimIndex = -1 then
    begin
     if GameObjectList.Count = 1 then
      AimIndex := 0
     else
      AimIndex := GameObjectList.Count -1;
    end;
end;

// ---------------------------------------------------------------------------//


// --------------------- { TBullet } -----------------------------------------//

constructor TBullet.Create(aim: TBaseObject; objImage: TAsphyreImage;
             SpeedNorm: integer);
begin
  inherited Create('bullet', objImage);
  Speed := (15.0 + Random * 40.0) / SpeedNorm;
  target := aim;
  TargetHit := False;
  Accuracy := 0.75;
end;

function TBullet.HitTest: boolean;
begin
  Randomize;
  Result := Random < Accuracy;
end;

procedure TBullet.Move;
const
  Eps = 1.;
var
  dx, dy: real;
begin
  dx := Target.Coord.x - Coord.x;
  dy := Target.Coord.y - Coord.y;
  if Abs(dx + dy) < Eps then
    begin
      // hit test
      {$WARN UNSAFE_CODE OFF}
      if @OnMiss <> nil then
        begin
          TargetHit := HitTest;
          if TargetHit then
            OnShot(Self, Self.Target)
          else
            OnMiss(Self, Self.Target);
        end
      else
        OnShot(Self, Self.Target)
      {$WARN UNSAFE_CODE ON}
    end;
    
  if not TargetHit then
    begin
      Coord.x := Coord.x + Cos(FlyAngle);
      Coord.y := Coord.y + Sin(FlyAngle);
    end;
end;

procedure TBullet.SetAccuracy(newAccuracy: real);
begin
  Accuracy := newAccuracy;
end;

procedure TBullet.SetFlyingAngle;
begin
  FlyAngle := ArcTan2(Target.Coord.y - Coord.y, Target.Coord.x - Coord.x);
end;

// ---------------------------------------------------------------------------//

// ------------------------ { TBulletGenerator }  ----------------------------//

constructor TBulletGenerator.Create(objImage: TAsphyreImage);
begin
  inherited Create(objImage);
  SpawnTimer.Interval := 600;
end;

procedure TBulletGenerator.SetFeeder(aimFeeder: TFeeder);
begin
  Feeder := aimFeeder;
end;

procedure TBulletGenerator.SetHitCallback(callback: TOnShot);
begin
  OnShot := callback;
end;

procedure TBulletGenerator.SetMissCallback(callback: TOnShot);
begin
  OnMiss := callback;
end;

procedure TBulletGenerator.Spawn;
var
  bullet: TBullet;
  aim: TBaseObject;
begin
  aim := Feeder();
  if aim <> nil then
  begin
    bullet := TBullet.Create(aim, Image, SpeedNorm);
    {$WARN UNSAFE_CODE OFF}
    if (@CoordGenerator <> nil) then
      begin
        bullet.Coord := CoordGenerator();
        bullet.SetFlyingAngle;
      end;
    if (@OnShot <> nil) then
      begin
        bullet.OnShot := OnShot;
      end;
    if (@OnMiss <> nil) then
      begin
        bullet.OnMiss := OnMiss;
      end;
    {$WARN UNSAFE_CODE ON}
    GameObjectList.Add(bullet);
  end;
end;

// ---------------------------------------------------------------------------//

end.
